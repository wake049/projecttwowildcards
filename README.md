# ProjectTwoWildCards




## Project Description

This project is a project management tool where managers can create projects and task. Then assign "employees" to those tasks. Both employees and managers can view the task in any project that they are assigned to and can change the information about that task. The limitation for employees is that they can not reassign employees. Projects also have images with them to give a visual on what each project includes

## Technolgies Used
spring-boot-starter-mail
pring-boot-starter-data-jpa
spring-boot-starter-web
spring-boot-devtools
spring-boot-starter-test
selenium-java 3.141.59
cucumber-java 6.9.1
cucumber-junit 6.9.1
junit-jupiter-api 5.7.0
mockito-all 1.10.19
postgresql
Visual Studio Code
Spring
Eclipse ide
DBeaver
AWS
HTML
Javascript
Java 8

## Features

Employees can view the projects assigned to them and view every task in those projects
A employee can then click on a task and update the status or the tag within it
Managers can create new projects and task and assign both to a employee
Managers can also do the same as an employee but can chnage the employees assigned to a task
Both managers and employees can reset passwords
New users can register
A email is sent when a employee tries to reset a password
