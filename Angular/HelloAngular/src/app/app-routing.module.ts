import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForgotpasswordComponent } from './components/forgotpassword/forgotpassword.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { RegisterComponent } from './components/register/register.component';
import { EmployeeProjectViewComponent } from './employee-project-view/employee-project-view.component';
import { CommonModule } from '@angular/common';
import { CreateProjectComponent } from './components/create-project/create-project.component';
import { ChangeTaskComponent } from './components/change-task/change-task.component';
import { TaskManagerComponent } from './components/task-manager/task-manager.component';
import { CreatetaskComponent } from './components/createtask/createtask.component';
import { ProjectViewComponent } from './project-view-manager/project-view.component';
import { ViewprojecttaskComponent } from './components/viewprojecttask/viewprojecttask.component';
import { ViewtaskemployeeComponent } from './components/viewtaskemployee/viewtaskemployee.component';
import { ChangetaskemployeeComponent } from './components/changetaskemployee/changetaskemployee.component';
import { TaskemployeeComponent } from './components/taskemployee/taskemployee.component';
import { EmailComponent } from './components/email/email.component';

const routes: Routes = [
  
   { path: 'login', component: UserLoginComponent},
   { path: 'login/:username', component: UserLoginComponent},
   {path: 'taskview', component:TaskManagerComponent}, 
   {path: 'taskview/:taskname', component:ChangeTaskComponent},
   {path: 'taskviewe', component:TaskemployeeComponent}, 
   {path: 'taskviewe/:taskname', component:ChangetaskemployeeComponent},
   {path: 'api', component:EmailComponent},
   {path: 'api/:username', component:EmailComponent},
  {path: 'forgotpassword', component :ForgotpasswordComponent},
  {path: 'employeehome', component :EmployeeProjectViewComponent},
  {path: 'register', component :RegisterComponent},
  
  {path: 'view/:projectName', component: ViewprojecttaskComponent},
  {path: 'viewe/:projectName', component: ViewtaskemployeeComponent},
  { path: 'login/change', component: UserLoginComponent},
  { path: 'taskcreate/change/new', component: ChangeTaskComponent},
  {path: 'create', component: CreateProjectComponent}, 
  
  {path: 'taskmanager', component: TaskManagerComponent},
  {path: 'taskmanagere', component: TaskemployeeComponent},  
  {path: ' ', redirectTo: '/login', pathMatch: 'full'},
  { path: '', component: NotFoundComponent}, 

  {path : 'task', component:  ProjectViewComponent},
    

   {path: 'taskcreate', component: CreatetaskComponent}

  
];
@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }