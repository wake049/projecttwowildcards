import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';


import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from './User.service';
import { UserModel } from './UserModel';

/* import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from './User.service';
import { UserModel } from './UserModel'; */
  


@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  userForm = new FormGroup({
    username:new FormControl(''),
    password: new FormControl('')
  });
  userModel:UserModel;

  constructor(private userServ: UserService, public router: Router, private actRoute?:ActivatedRoute) { }

  ngOnInit():void{
    localStorage.removeItem("loggedUser");
  }
  public login(userForm: FormGroup){
    let user = JSON.stringify(userForm.value);
    let username = this.userForm.get("username").value;
    this.userServ.postUserByUsername(user).subscribe(
      respone =>{
        this.getUserInfo(username);
        //this.router.navigate(['/task']);
      },
      error =>{
        console.warn("username or password does not exist");
      }
    );
  }
  public getUserInfo(username: string){
    this.userServ.getUserInfo(username).subscribe(
      response =>{
        this.router.navigate(['/user/:' + username]);
        this.userModel =response;
                          
        localStorage.setItem("loggedUser", JSON.stringify(this.userModel));
        if(this.userModel.userRole == 1){// manager page
          this.router.navigate(['/task']);
        }
        else if(this.userModel.userRole == 2){// employee page

          this.router.navigate(['/employeehome']);
        }
      },
      erro =>{
        console.log(username);
        console.warn("could not find");
      }
    );
  }
}

