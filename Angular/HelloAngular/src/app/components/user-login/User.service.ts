import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserLogin } from './user';
import { UserModel } from './UserModel';



@Injectable({
  providedIn: 'root'
})
export class UserService {
  getAllUserModel() {
    throw new Error('Method not implemented.');
  }
  getAllUser() {
    throw new Error('Method not implemented.');
  }

  private urlBase = "http://localhost:9047/login";
  httpHead = {
    headers: new HttpHeaders({
      'Content-Type':'application/json',
      'Access-Control-Allow-Origin':'*'
    })
  };

  constructor(private http: HttpClient) { }
  //CORS - Cross Origin Resource Sharing

  public postUserByUsername(user :string): Observable<UserLogin>{
    return this.http.post<UserLogin>(this.urlBase, user, this.httpHead);
  }
  public getUserInfo(user :string):Observable<UserModel>{
    return this.http.get<UserModel>(this.urlBase + "/" + user, this.httpHead);
  }

  public changePassword(password:string):Observable<UserModel>{
    return this.http.patch<UserModel>(this.urlBase + "/" +password , this.http);
  }
}