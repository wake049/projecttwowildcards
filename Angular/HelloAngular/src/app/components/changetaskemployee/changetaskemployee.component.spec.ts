import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangetaskemployeeComponent } from './changetaskemployee.component';

describe('ChangetaskemployeeComponent', () => {
  let component: ChangetaskemployeeComponent;
  let fixture: ComponentFixture<ChangetaskemployeeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangetaskemployeeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangetaskemployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
