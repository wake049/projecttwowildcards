import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ChangeTaskService } from '../change-task/changetaskservice';
import { UserModel } from '../create-project/createUserModel';
import { TaskModel } from '../task-manager/task-manager';

@Component({
  selector: 'app-changetaskemployee',
  templateUrl: './changetaskemployee.component.html',
  styleUrls: ['./changetaskemployee.component.css']
})
export class ChangetaskemployeeComponent implements OnInit {


  fNameList: UserModel[] = [];
  finalfName: string[] = [];
  taskUser: string[] = [];
  task_name: string;
  task: TaskModel;
  taskForm = new FormGroup({
    Status: new FormControl(''),
    tag: new FormControl(''),
    Priority:new FormControl('')
  });
  userrole:boolean;
  loggedUser: string;

  constructor(private changeServ: ChangeTaskService, private actRoute: ActivatedRoute, private router: Router) { }
  ngOnInit(): void {
    this.task_name = this.actRoute.snapshot.paramMap.get('taskname');
    this.changeServ.getTaskByName(this.task_name).subscribe(
      response => {
        this.task = response;
        for (let user of this.task.useList) {
          this.taskUser.push(user.fName);
          this.finalfName.push(user.fName);
          console.log(this.finalfName)
        }

      }

    );
    this.changeServ.getAllUsers().subscribe(
      response => {
        this.fNameList = response;
      },
      error => {
        console.warn("Not working");
      }
    );
    let user = JSON.parse(localStorage.getItem("loggedUser"));
    console.log(user + "user");
    if(!user){
      this.router.navigate(["/login"]);
    }
    if(user.userRole == "1"){
      this.userrole = true;
    }
    if(user.userRole == "2"){
      this.userrole = false;
    }

  }
  public onChecked(obj: any, isChecked: boolean) {
    let index;
    if (!isChecked) {
      index = this.finalfName.indexOf(obj);
      this.finalfName.splice(index, 1);
      for (let user of this.finalfName){
      
      this.changeServ.postUsers(user).subscribe(
        response=>{
          console.log(this.finalfName + "user");
        },
        error=>{
          
          console.log(this.finalfName) + "error";
        }
  
      );
    }
    }
    else {
      this.finalfName.push(obj);
      for (let user of this.finalfName){
      this.changeServ.postUsers(user).subscribe(
        response=>{
          console.log(this.finalfName + "user");
        },
        error=>{
          
          console.log(this.finalfName) + "error";
        }
  
  
      );
    }
      this.finalfName.push(obj);
    }
  }
  public submit(taskForm: FormGroup) {
    this.router.navigate(['/taskcreate/change/new']);
    let task = JSON.stringify(taskForm.value);
    this.changeServ.postUsers(task).subscribe(
      response => {
    this.router.navigate(['/taskviewe']);
        console.log(task);
      },
      error=>{
        this.router.navigate(['/taskviewe']);
        console.log(task);
      }
    );


  }
  loggedin(){
    this.loggedUser=localStorage.getItem('token');
    return this.loggedUser;
  }
  onLogout(){
    localStorage.removeItem('loggedUser');
    this.router.navigate(["/login"]).then(()=>{
      
    })
    
    
  }

}
