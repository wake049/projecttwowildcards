import { TaskModel } from "../task-manager/task-manager";

export class ProjectTaskModel{
    constructor(public taskList:TaskModel[]){}
}