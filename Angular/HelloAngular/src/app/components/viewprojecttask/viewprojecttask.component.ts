import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskModel } from '../task-manager/task-manager';
import { ProjectTaskModel } from './ProjectTaskModel';
import { ProjectTaskService } from './TaskprojectService';

@Component({
  selector: 'app-viewprojecttask',
  templateUrl: './viewprojecttask.component.html',
  styleUrls: ['./viewprojecttask.component.css']
})
export class ViewprojecttaskComponent implements OnInit {
projectname:string;
task:ProjectTaskModel;
//taskList:String[];
  loggedUser: string;
  router: any;

  constructor(private actRoute: ActivatedRoute, private pServ:ProjectTaskService, router:Router) { }

  ngOnInit(): void {
    this.projectname = this.actRoute.snapshot.paramMap.get('projectName');
    this.pServ.getProjectByName(this.projectname).subscribe(
      response => {
        this.task = response;
        console.log(this.task.taskList);
      }
    )}
    loggedin(){
      this.loggedUser=localStorage.getItem('token');
      return this.loggedUser;
    }
    onLogout(){
      localStorage.removeItem('loggedUser');
      this.router.navigate(["/login"]).then(()=>{
        
      })
      
      
    }
}
