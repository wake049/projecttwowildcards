import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewprojecttaskComponent } from './viewprojecttask.component';

describe('ViewprojecttaskComponent', () => {
  let component: ViewprojecttaskComponent;
  let fixture: ComponentFixture<ViewprojecttaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewprojecttaskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewprojecttaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
