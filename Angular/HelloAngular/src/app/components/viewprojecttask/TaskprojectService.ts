import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ProjectTaskModel } from "./ProjectTaskModel";

@Injectable({
    providedIn: 'root'
  })
  export class ProjectTaskService {
  
    private urlBase = "http://localhost:9047/projects";
    httpHead = {
      headers: new HttpHeaders({
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*'
      })
    };
  
    constructor(private http: HttpClient) { }

        public getProjectByName(name:string):Observable<ProjectTaskModel>{
            return this.http.get<ProjectTaskModel>(this.urlBase +"/"+name, this.httpHead);
        }
}