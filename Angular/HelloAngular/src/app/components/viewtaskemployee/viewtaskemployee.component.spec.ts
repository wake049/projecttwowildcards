import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewtaskemployeeComponent } from './viewtaskemployee.component';

describe('ViewtaskemployeeComponent', () => {
  let component: ViewtaskemployeeComponent;
  let fixture: ComponentFixture<ViewtaskemployeeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewtaskemployeeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewtaskemployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
