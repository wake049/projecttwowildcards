import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectTaskModel } from '../viewprojecttask/ProjectTaskModel';
import { ProjectTaskService } from '../viewprojecttask/TaskprojectService';

@Component({
  selector: 'app-viewtaskemployee',
  templateUrl: './viewtaskemployee.component.html',
  styleUrls: ['./viewtaskemployee.component.css']
})
export class ViewtaskemployeeComponent implements OnInit {
  projectname:string;
  task:ProjectTaskModel;
  taskList:String[];
  loggedUser: string;
  
  
    constructor(private actRoute: ActivatedRoute, private pServ:ProjectTaskService, private router:Router) { }
  
    ngOnInit(): void {
      this.projectname = this.actRoute.snapshot.paramMap.get('projectName');
      this.pServ.getProjectByName(this.projectname).subscribe(
        response => {
          this.task = response;
          console.log(this.task.taskList);
        }
      )}
  
      loggedin(){
        this.loggedUser=localStorage.getItem('token');
        return this.loggedUser;
      }
      onLogout(){
        localStorage.removeItem('loggedUser');
        this.router.navigate(["/login"]).then(()=>{
          
        })
        
        
      }
  }
