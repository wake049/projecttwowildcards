import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { register } from "./register";


@Injectable({
    providedIn: 'root'
  })
  export class RegisterService {
  
    private urlBase = "http://localhost:9047/user";
    httpHead = {
      headers: new HttpHeaders({
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*'
      })
    };
    constructor(private http: HttpClient) { }
    public insertUser(user:string): Observable<register>{
        return this.http.post<register>(this.urlBase, user, this.httpHead);
       }
}