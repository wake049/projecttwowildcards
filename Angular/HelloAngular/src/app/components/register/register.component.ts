import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { Router } from '@angular/router';
import { RegisterService } from './registerservice';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm = new FormGroup({
    fname: new FormControl(''),
    lname: new FormControl(''),
    username: new FormControl(''),
    password: new FormControl('')

  });
  constructor(private registerServ: RegisterService, private router: Router) { }

  ngOnInit(): void {

  }

  public register(registerForm: FormGroup) {
    let user = JSON.stringify(registerForm.value);
    this.registerServ.insertUser(user).subscribe(
      response => {
        console.log("user was inserted");
        this.router.navigate(["/login"]);
      },
      error => {
        console.log("username alreay exist");
      }

    )
  }

}
