import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TaskModel } from './task-manager';
import { TaskManagerService } from './task-manager.service';
import { UserModelTask } from './UserModel';

@Component({
  selector: 'app-task-manager',
  templateUrl: './task-manager.component.html',
  styleUrls: ['./task-manager.component.css']
})
export class TaskManagerComponent implements OnInit {
  taskList:TaskModel[] = [];
  fName:String[] = [];
  loggedUser: string;
  constructor(private taskServ:TaskManagerService,private router:Router) { }

  ngOnInit(): void {
     this.taskServ.getAllTask().
     subscribe(
      response =>{
        console.log(response);
        this.taskList=response;
        for(let user of this.taskList[0].useList){
          console.log(user.fName);
        }
        console.log(this.taskList[0].useList)
          //this.fName = value.userList.fName;
        }); 
        //console.log(this.taskList);
      }
      loggedin(){
        this.loggedUser=localStorage.getItem('token');
        return this.loggedUser;
      }
      onLogout(){
        localStorage.removeItem('loggedUser');
        this.router.navigate(["/login"]).then(()=>{
          
        })
        
        
      }

}

