import { UserModel } from "../user-login/UserModel";
import { PrioModel } from "./PrioModel";
import { StatusModel } from "./StatusModel";
import { UserModelTask } from "./UserModel";

export class TaskModel{
    constructor(public taskId:number, public taskName:string, public description:string, public statusTask:string, 
        public tag:string, public dueDate:Date, public priority:string, public useList:UserModelTask[]){
        
    }
    
    }
    