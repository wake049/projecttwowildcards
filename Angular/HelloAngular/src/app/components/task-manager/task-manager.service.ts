import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TaskModel } from './task-manager';
import { TaskManagerComponent } from './task-manager.component';

@Injectable({
  providedIn: 'root'
})
export class TaskManagerService {
  private urlBase = "http://localhost:9047/task";
   httpHead = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };

  constructor(private http: HttpClient) { }

  public getAllTask(): Observable<TaskModel[]> {
    return this.http.get<TaskModel[]>(this.urlBase, this.httpHead);
    
   }
   public insertTaskModel(taskmodel:string):Observable<TaskModel>{
     return this.http.post<TaskModel>(this.urlBase, taskmodel, this.httpHead);

   }
  }
 
 
