import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ProjectComponent } from "src/app/projectmanagement/member-dashboard/project/project.component";
import { RecentComponent } from "src/app/projectmanagement/member-dashboard/recent/recent.component";
import { StarredComponent } from "src/app/projectmanagement/member-dashboard/starred/starred.component";
import { TaskComponent } from "src/app/projectmanagement/member-dashboard/task/task.component";
import { ProjectmanagementComponent } from "src/app/projectmanagement/projectmanagement.component";
import { NavComponent } from "./nav.component";


const routes: Routes= [
    {path:'', component:NavComponent,
    children:[

        {path:'project', component: ProjectComponent},
        {path: 'Recent', component: RecentComponent },
        {path: 'starred', component: StarredComponent },
        {path: 'task', component: TaskComponent },
        {path: '', redirectTo: '/memberdashboard/home',pathMatch: 'full' },
    ],
    },
     ];

     @NgModule({

        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
        
         })
         export class projectmanagementComponent{}