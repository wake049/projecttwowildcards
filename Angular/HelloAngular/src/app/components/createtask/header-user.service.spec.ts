import { TestBed } from '@angular/core/testing';

import { HeaderUserService } from './header-user.service';

describe('HeaderUserService', () => {
  let service: HeaderUserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HeaderUserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
