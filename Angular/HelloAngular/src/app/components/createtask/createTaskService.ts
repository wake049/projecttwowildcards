import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProjectModel } from '../create-project/createproject';
import { CreateTask } from './createTaskModel';


@Injectable({
  providedIn: 'root'
})
export class CreateTaskService {
  private urlBase = "http://localhost:9047/create";
   httpHead = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };

  constructor(private http: HttpClient) { }

   public getAllProjects(): Observable<ProjectModel[]>{
    return this.http.get<ProjectModel[]>(this.urlBase, this.httpHead);
  }
  public postTask(user:string): Observable<CreateTask> {
    return this.http.post<CreateTask>(this.urlBase, user, this.httpHead);
   }
}