import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ChangeTaskService } from '../change-task/changetaskservice';
import { TaskModelChange } from '../change-task/TaskModel';
import { ProjectModel } from '../create-project/createproject';
import { TaskModel } from '../task-manager/task-manager';
import { UserModel } from '../user-login/UserModel';
import { CreateTask } from './createTaskModel';
import { CreateTaskService } from './createTaskService';

@Component({
  selector: 'app-createtask',
  templateUrl: './createtask.component.html',
  styleUrls: ['./createtask.component.css']
})
export class CreatetaskComponent implements OnInit {

  fNameList: ProjectModel[] = [];
  finalfName: string;
  taskUser: string[] = [];
  task_name: string;
  TaskModel: CreateTask;
  taskForm = new FormGroup({
    statusTask: new FormControl(''),
    tag: new FormControl(''),
    priorModel:new FormControl(''),
    taskName:new FormControl(''),
    projectName:new FormControl(''),
    description:new FormControl(''),
    dueDate:new FormControl('')
  });
  loggedUser: string;

  constructor(private changeServ: CreateTaskService, private actRoute: ActivatedRoute, private router: Router) { }
  ngOnInit(): void {
    this.changeServ.getAllProjects().subscribe(
      response => {
        this.fNameList = response;
        console.log(this.fNameList);
      },
      error => {
        console.warn("Not working");
      }
    );
  }
  public submit(taskForm: FormGroup) {
    this.router.navigate(['/taskcreate']);
    let task = JSON.stringify(taskForm.value);
    this.changeServ.postTask(task).subscribe(
      response => {
    this.router.navigate(['/taskview']);
        console.log(task);
      },
      error=>{
        this.router.navigate(['/taskview']);
        console.log(task);
      }
    );


  }

  loggedin(){
    this.loggedUser=localStorage.getItem('token');
    return this.loggedUser;
  }
  onLogout(){
    localStorage.removeItem('loggedUser');
    this.router.navigate(["/login"]).then(()=>{
      
    })
    
    
  }
}
