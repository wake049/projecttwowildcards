import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserModel } from './header';


@Injectable({
  providedIn: 'root'
})
export class HeaderUserService {
private urlBase   ="http://localhost:9047/user";

 httpHead ={
  headers : new HttpHeaders({
   'Content-Type':'application/json',
   'Access-Control-Allow-Origin':'*'

  })
    };
  constructor(private http: HttpClient) { }
  public getAllUserModel():Observable<UserModel[]>{
  
       return this.http.get<UserModel[]>(this.urlBase, this.httpHead);
  }
  public insertUserModel(usermodel:string):Observable<UserModel>{
  return this.http.post<UserModel>(this.urlBase, usermodel,this.httpHead);
    
}


}