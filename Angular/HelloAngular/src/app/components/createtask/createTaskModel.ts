export class CreateTask{
    constructor(public TaskName:string, public TaskStatus:string, public TaskDueDate:string, public TaskTag:string, public ProjectName:string, public Priority:string){}
}
