import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ForgotService } from './ForgotService';


@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  userForm = new FormGroup({
    username:new FormControl(''),
    password: new FormControl('')
  });

  constructor(private forgotService: ForgotService, public router: Router) { }

  ngOnInit(): void {

  }

  public changePassword(userForm:FormGroup){
    console.log("change");
    this.router.navigate(['/login/change'])
    let user = JSON.stringify(userForm.value);
    this.forgotService.changePassword(user).subscribe(
      response =>{
        this.router.navigate(["/login"]);
      },
      error=>{
        console.log(user);
        console.warn("could not find");
      }

    );
  }


}
