import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserChange } from './Forgot';





@Injectable({
    providedIn: 'root'
})
export class ForgotService {

    private urlBase = "http://localhost:9047/login";
    httpHead = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        })
    };

    constructor(private http: HttpClient) { }
    //CORS - Cross Origin Resource Sharing

    public changePassword(user: string): Observable<UserChange> {
        return this.http.post<UserChange>(this.urlBase +"/change", user, this.httpHead);
    }
}