import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserModel } from '../create-project/createUserModel';
import { TaskModel } from '../task-manager/task-manager';
import { TaskModelChange } from './TaskModel';


@Injectable({
  providedIn: 'root'
})
export class ChangeTaskService {
  private urlBase = "http://localhost:9047/taskcreate";
  private urlBase1 = "http://localhost:9047/taskcreate/change/new";
   httpHead = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };

  constructor(private http: HttpClient) { }

  public getTaskByName(name:string): Observable<TaskModel> {
    return this.http.get<TaskModel>(this.urlBase + "/" +name, this.httpHead);
   }
   public getAllUsers(): Observable<UserModel[]>{
    return this.http.get<UserModel[]>(this.urlBase, this.httpHead);
  }
  public postUsers(user:string): Observable<string> {
    return this.http.post<string>(this.urlBase, user, this.httpHead);
   }
   public postStatus(user:string, name: string): Observable<TaskModelChange> {
    return this.http.post<TaskModelChange>(this.urlBase1, {user, name}, this.httpHead);
   }
}