import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserModel } from '../create-project/createUserModel';
import { TaskModel } from '../task-manager/task-manager';
import { ChangeTaskService } from './changetaskservice';
import { TaskModelChange } from './TaskModel';

@Component({
  selector: 'app-change-task',
  templateUrl: './change-task.component.html',
  styleUrls: ['./change-task.component.css']
})
export class ChangeTaskComponent implements OnInit {

  fNameList: UserModel[] = [];
  finalfName: string[] = [];
  taskUser: string[] = [];
  task_name: string;
  task: TaskModel;
  taskChange: TaskModelChange;
  taskForm = new FormGroup({
    Status: new FormControl(''),
    tag: new FormControl(''),
    Priority:new FormControl('')
  });
  userrole:boolean;
  loggedUser: string;

  constructor(private changeServ: ChangeTaskService, private actRoute: ActivatedRoute, private router: Router) { }
  ngOnInit(): void {
    this.task_name = this.actRoute.snapshot.paramMap.get('taskname');
    this.changeServ.getTaskByName(this.task_name).subscribe(
      response => {
        this.task = response;
        for (let user of this.task.useList) {
          this.taskUser.push(user.fName);
          this.finalfName.push(user.fName);
          console.log(this.finalfName)
        }

      }

    );
    this.changeServ.getAllUsers().subscribe(
      response => {
        this.fNameList = response;
      },
      error => {
        console.warn("Not working");
      }
    );

  }
  public onChecked(obj: any, isChecked: boolean) {
    let index;
    if (!isChecked) {
      index = this.finalfName.indexOf(obj);
      this.finalfName.splice(index, 1);
    }
    else{
      this.finalfName.push(obj);
    }

  }


  public submit(taskForm: FormGroup) {
    
    this.router.navigate(['/taskcreate/change/new']);
    let task = JSON.stringify(taskForm.value);
    let Name:string =  JSON.stringify(this.finalfName);
    this.changeServ.postStatus(task, Name).subscribe(
      response => {
    this.router.navigate(['/taskview']);
      },
      error=>{
        this.router.navigate(['/taskview'])
      }
    );


  }
  loggedin(){
    this.loggedUser=localStorage.getItem('token');
    return this.loggedUser;
  }
  onLogout(){
    localStorage.removeItem('loggedUser');
    this.router.navigate(["/login"]).then(()=>{
      
    })
    
    
  }

}
