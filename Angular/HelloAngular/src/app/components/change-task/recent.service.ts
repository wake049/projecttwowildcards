import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
 //import { TaskModel } from './recent'; 


@Injectable({
  providedIn: 'root'
})
export class RecentService {

 private urlBase = "http://localhost:9047/task";
   httpHead = {
  headers: new HttpHeaders({
    'Content-Type':'application/json',
    'Access-Control-Allow-Origin':'*'
    
  })
};

  constructor(private http: HttpClient) { }

  /* public getAllTaskModel():Observable<TaskModel[]>{
    return this.http.get<TaskModel[]>(this.urlBase,this.httpHead);

    }
  public insertTaskModel(taskmodel:string):Observable<TaskModel>{
     return this.http.post<TaskModel>(this.urlBase, taskmodel, this.httpHead);
  }
   public getTaskByName(task:string):Observable<TaskModel>{
    return this.http.get<TaskModel>(this.urlBase+"/"+task, this.httpHead);
  }
 
 public deleteTaskByName(task:string):Observable<TaskModel>{
 
   return this.http.delete<TaskModel>(this.urlBase+"/"+task,this.httpHead);
 }  */
}
