import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmailModel } from './Email';





@Injectable({
    providedIn: 'root'
})
export class SendEmail {

    private urlBase = "http://localhost:9047/api";
    httpHead = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        })
    };

    constructor(private http: HttpClient) { }
    //CORS - Cross Origin Resource Sharing

    public sendEmail(user: string): Observable<EmailModel> {
        return this.http.get<EmailModel>(this.urlBase+"/"+ user, this.httpHead);
    }
}