import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { SendEmail } from './EmailService';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {


  userForm = new FormGroup({
    username:new FormControl('')
  });


  constructor(private eServ: SendEmail, private router:Router) { }

  ngOnInit(): void {
  }
  public changePassword(userForm:FormGroup){
    let username = userForm.get('username').value;
    this.router.navigate(['/api/' + username]);
    this.eServ.sendEmail(username).subscribe(
      response=>{
      console.log("sent");
      })
  }
}
