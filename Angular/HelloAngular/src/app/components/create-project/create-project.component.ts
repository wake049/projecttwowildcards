import { Component, OnInit } from '@angular/core';
import { CheckboxControlValueAccessor, CheckboxRequiredValidator, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user-login/User.service';
import { ProjectService } from './createprojectservice';
import { UserModel } from './createUserModel';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent implements OnInit {

  fNameList:UserModel[] = [];
  finalFName:string;
  finalfName:string[] = [];
  isChecked:boolean;
  obj:any;

  projectForm = new FormGroup({
    projectName:new FormControl,
    description:new FormControl,
    dueDate:new FormControl,
    file:new FormControl
  })
  loggedUser: string;

  constructor(private projectServ:ProjectService, private router: Router) { }

  ngOnInit(): void {
    this.projectServ.getAllUsers().subscribe(
      response=>{
        this.fNameList =response;
        for(let fname of this.fNameList){
          console.log(fname.fName);
        }
      },
      error=>{
        console.warn("Not working");
      }
    );

  }

  onChecked(obj: any, isChecked: boolean){
    console.log(obj, isChecked); // {}, true || false
    if(isChecked){
      this.finalfName.push(obj);
      console.log(this.finalfName)
    }
    else{
      let index = this.finalfName.indexOf(obj);
      this.finalfName.splice(index, 1);
      console.log(this.finalfName)
    }

  }

  public submit(projectForm: FormGroup){
    let user = JSON.stringify(projectForm.value);
    let Name:string =  JSON.stringify(this.finalfName);
    this.projectServ.createProject(user, Name).subscribe(
        response=>{
          console.log(user);
          this.router.navigate(["/task"])
        },
        error=>{
          console.log("failed")
        }
    );
  }
  loggedin(){
    this.loggedUser=localStorage.getItem('token');
    return this.loggedUser;
  }
  onLogout(){
    localStorage.removeItem('loggedUser');
    this.router.navigate(["/login"]).then(()=>{
      
    })
    
    
  }
}
