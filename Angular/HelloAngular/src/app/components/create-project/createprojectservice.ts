import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProjectModel } from './createproject';
import { UserModel } from './createUserModel';



@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private urlBase = "http://localhost:9047/projectscreate";
  httpHead = {
    headers: new HttpHeaders({
      'Content-Type':'application/json',
      'Access-Control-Allow-Origin':'*'
    })
  };

  constructor(private http: HttpClient) { }
  //CORS - Cross Origin Resource Sharing

  public createProject(project :string,  userFname:string): Observable<ProjectModel>{
    return this.http.post<ProjectModel>(this.urlBase, {project, userFname}, this.httpHead);
  }
  public getAllUsers(): Observable<UserModel[]>{
    return this.http.get<UserModel[]>(this.urlBase, this.httpHead);
  }
}