import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { window } from 'rxjs';
import { ProjectModel } from './project-view';
import { ProjectViewService } from './project-view.service';

@Component({
  selector: 'app-project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.css']
})
export class ProjectViewComponent implements OnInit {

  projectList: ProjectModel[] = [];
  loggedUser: string;

  constructor(private projViewServ:ProjectViewService,private router:Router,private actRoute:ActivatedRoute) { }

  ngOnInit(): void {
    this.projViewServ.getAllProjects().subscribe(
      response => {
        console.log(response);
        this.projectList=response;
      }
    );
  }
  
  loggedin(){
    this.loggedUser=localStorage.getItem('token');
    return this.loggedUser;
  }
  onLogout(){
    localStorage.removeItem('loggedUser');
    this.router.navigate(["/login"]).then(()=>{
      
    })
    
    
  }


}
