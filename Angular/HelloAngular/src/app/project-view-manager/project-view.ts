import { ImageModel } from "../employee-project-view/ImageObject";

export class ProjectModel{
    constructor(public projectId:number, public projectName:string, public description:string, public dueDate:Date, public imgName?:ImageModel, public userList?:string, public taskList?:string) {}
}