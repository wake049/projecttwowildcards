import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProjectModel } from './project-view';

@Injectable({
  providedIn: 'root'
})
export class ProjectViewService {

  private urlBase = "http://localhost:9047/projects";

  constructor(private http:HttpClient) { }

  public getAllProjects(): Observable<ProjectModel[]> {
    const httpHead = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    };
    return this.http.get<ProjectModel[]>(this.urlBase, httpHead);
  }


}
