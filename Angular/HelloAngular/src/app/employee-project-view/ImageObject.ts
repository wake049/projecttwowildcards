import { Byte } from "@angular/compiler/src/util";

export class ImageModel{
    constructor(public id:string, public name:string, public contentType:string, public size:number, public data:Blob[]){}
}