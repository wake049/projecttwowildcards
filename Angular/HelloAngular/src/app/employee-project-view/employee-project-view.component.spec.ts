import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeProjectViewComponent } from './employee-project-view.component';

describe('EmployeeProjectViewComponent', () => {
  let component: EmployeeProjectViewComponent;
  let fixture: ComponentFixture<EmployeeProjectViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeProjectViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeProjectViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
