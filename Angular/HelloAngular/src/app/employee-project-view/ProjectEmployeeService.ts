import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ProjectEmployee } from "./project";

@Injectable({
    providedIn: 'root'
  })
  export class ProjectEmployeeService {
  
    private urlBase = "http://localhost:9047/projects";
    httpHead = {
      headers: new HttpHeaders({
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*'
      })
    };
  
    constructor(private http: HttpClient) { }

        public getAllProject():Observable<ProjectEmployee[]>{
            return this.http.get<ProjectEmployee[]>(this.urlBase, this.httpHead);
        }
}
