import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
/* import { ProjectService } from '../projectmanagement/project.service'; */
import { ProjectEmployee } from './project';
import { ProjectEmployeeService } from './ProjectEmployeeService';
import { DomSanitizer } from '@angular/platform-browser';
import { ImageModel } from './ImageObject';

@Component({
  selector: 'app-employee-project-view',
  templateUrl: './employee-project-view.component.html',
  styleUrls: ['./employee-project-view.component.css']
})
export class EmployeeProjectViewComponent implements OnInit {

  projectList: ProjectEmployee[] = [];
  img: File[];
  loggedUser: any;
  imagePath;


  constructor(private router: Router, private pServ: ProjectEmployeeService,private actRoute:ActivatedRoute, private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    let user = JSON.parse(localStorage.getItem("loggedUser"));
    console.log(user + "user");
    if(!user){
      this.router.navigate(["/login"]);
    }
    this.pServ.getAllProject().subscribe(
      response=>{
        this.projectList=response;

      },
      error=>{
        console.log(this.projectList[0].imgName)
      }

    );

  }
  loggedin(){
    this.loggedUser=localStorage.getItem('token');
    return this.loggedUser;
  }
  onLogout(){
    localStorage.removeItem('loggedUser');
    this.router.navigate(["/login"]).then(()=>{
      
    })
    
    
  }


}
