import { ImageModel } from "./ImageObject";

export class ProjectEmployee{
    constructor(public projectName: string, public imgName:ImageModel){}
}