import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import { AppComponent } from './app.component';
import { UserLoginComponent } from './components/user-login/user-login.component';
import {HttpClientModule} from '@angular/common/http';
import { NotFoundComponent } from './components/not-found/not-found.component';
 import { ForgotpasswordComponent } from './components/forgotpassword/forgotpassword.component';
import { RegisterComponent } from './components/register/register.component';

import { RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { EmployeeProjectViewComponent } from './employee-project-view/employee-project-view.component';
import { CreateProjectComponent } from './components/create-project/create-project.component';
import { ChangeTaskComponent } from './components/change-task/change-task.component';
import { TaskManagerComponent } from './components/task-manager/task-manager.component';
import { CreatetaskComponent } from './components/createtask/createtask.component';
import { ProjectViewComponent } from './project-view-manager/project-view.component';
import { ViewprojecttaskComponent } from './components/viewprojecttask/viewprojecttask.component';
import { ChangetaskemployeeComponent } from './components/changetaskemployee/changetaskemployee.component';
import { ViewtaskemployeeComponent } from './components/viewtaskemployee/viewtaskemployee.component';
import { TaskemployeeComponent } from './components/taskemployee/taskemployee.component';
import { EmailComponent } from './components/email/email.component';




@NgModule({
  declarations: [
    AppComponent,
    UserLoginComponent,
    NotFoundComponent,
    ForgotpasswordComponent,
    RegisterComponent,
    EmployeeProjectViewComponent,
    ProjectViewComponent,
    RegisterComponent,
    CreateProjectComponent,
    ProjectViewComponent,
    ChangeTaskComponent,
    TaskManagerComponent,
    CreatetaskComponent,
    ViewprojecttaskComponent,
    ChangetaskemployeeComponent,
    ViewtaskemployeeComponent,
    TaskemployeeComponent,
    EmailComponent

  ],
  imports: [
    BrowserModule, 
    AppRoutingModule, 
    FormsModule, 
    ReactiveFormsModule, 
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
