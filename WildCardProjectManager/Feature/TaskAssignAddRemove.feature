
Feature: Project managers can assign tasks and add or remove members 
 
 
		Background: A user is logged in to the Wildcard Project Manger App
				Given The user is on the login page
				When the user inputs "kamau.amen@revature.net" into the username field
				And the user inputs "12345" in to password field
				When the user clicks login
				Then the user is directed the App main page


	Scenario: Creating a project #new
					Given that a manager selects create a project
					And we arrive at the create project page
					When the user inputs "Operation Ardent" in Project Name 
					And the user inputs "This is a brief description" in Description
					And the user inputs "2022-10-11" in Due Date
					And the user checks the checkbox to add a team member
					When the user clicks submit
					Then the user is directed the App main page
				
				
		Scenario: Creating a new task in a project #new
					Given that a manager selects create a task
					And we arrive at the create task page
					When the user inputs "Sponsorshsip calls" in Task Name 
					And the user inputs "Team memebers should connect with a min of 10 contacts weekly" in Description
					And the user selects the current status from the Status dropdown
					And the user inputs "2022-05-06" in Due Date
					And the user selects the current priority from the Priority dropdown
					And the user selects the current priority from the Project dropdown
					And the user inputs "NTIP" in Tag box
					When the user clicks the submit button
					Then the user is directed the App main page
			
		
			Scenario: Viewing current tasks across projects #new
					Given that a manager selects view task
					And we arrive at the view task page	
					Then all tasks should be displayed
								
								
			Scenario: Updating current tasks in a project #new
					Given that a manager selects view task
					And we arrive at the view task page	
					And all tasks should be displayed	
					When user clicks on link in first task displayed
					And user arrives at the task edit page
					And the user selects the current status from the Status dropdown
					And the user selects the current priority from the Priority dropdown
					And the user checks the checkbox to add a team member				
					And the user inputs "UCTP" in Tag box
					When the user clicks submit task button
					Then all tasks should be displayed
					

		#Scenario: Assigning project tasks
			    #Given that the project manager is at the project view page
			  	#And the project manager selects wants to assign new tasks
			  	#When the project manager assigns a task by outling details
			  	#When the manager hits enter the manager is prompted for other entries/assigments
			  	#When the manager is complete
			    #And the team members gets notified
			    #Then the project manager is returned to the project view 
			    

	  #Scenario: Add members
			    #Given That I can view all projects
			    #And I am logged on as a manager
			    #When I click on a project
			    #And I drag and drop a employee into it
			    #Then I can see the employee is added to the project
    

	  #Scenario: Remove members
			   #Given That I can view all projects
			    #And I am logged on as a manager
			    #When I click on a project
			    #And I drag and drop a employee out of it
			    #Then I can see the employee is remove from the project
