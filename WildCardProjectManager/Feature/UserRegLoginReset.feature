Feature:  New users can register and login, current users can login, and password can be reset


				Background: A user is at the home page
								Given user on the homepage  
								And user selects the register new user link
								Then the user is directed to the register user page


				Scenario: Create a New User 
								Given the user fills in the first name "Kirk"
								And the user fills in the last name "Kingsley"
								And the user fills in their email "krking@email.com"
								And the user fills in their password "password"
								And the user repeats filling in their password "password"							
								And user clicks register the button
								Then user will be taken to home page to login as a new users


				#Scenario: User does not follow form validations
								#Given user enters wrong characters
								#And an error message displays with invalid credentials
								#Then user is sent back on registration page
				


				Scenario: Successful login function  
								Given user on the homepage for login
								And user enters their email as "akljdsghflk@ajkhdf.net" 
								And user enters "12345"   
								And user click login button
								Then user should be directed to the applications main page 
								

				Scenario: Unsuccessful login
								Given user on the homepage for login
								And user enters their email as "taylor.mac@email.com" 
								And user enters "45678"
								And user clicks login button
								And error message displayed with wrong password
								Then user is sent back on registration page


				Scenario: Successful logout
								Given user is inside the application 
								And user selects the logout feature
								Then all sessions are invalidated
								Then user is sent back to the login page
		
		
				Scenario: User Resets Password
						    Given user on the homepage for login
						    And the user selects the forgot password link
						    When the user is directeed to the reset password page
						    And the user fills in their username "kramen@gmail.com"
						    And the user fills in a new password "56789" they would like to use
						    And the user clicks the submit button 
						    Then the user should be directed back to the login page
						    And the user can use their newly created password to log in

	