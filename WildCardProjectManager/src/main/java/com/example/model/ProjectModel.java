package com.example.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.example.custom.ProjectCustom;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name="project")
@JsonSerialize(using = ProjectCustom.class)
public class ProjectModel {
	
	@Id
	@Column(name ="project_Id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int projectId;
	
	
	@Column(name ="projectName")
	private String projectName;
	
	@Column(name="description")
	private String description;
	
	@Column(name = "dueDate")
	private LocalDate dueDate;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@userId")
	private List<UserModel> userList;
	
	
	@ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@taskId")
	private List<TaskModel> taskList;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "imageModel")
	private ImageModel imgMod;
	
	public ProjectModel() {
		// TODO Auto-generated constructor stub
	}
	
	


	public ProjectModel(int projectId, String projectName, String description, LocalDate dueDate,
			List<UserModel> userList, List<TaskModel> taskList, ImageModel imgMod) {
		super();
		this.projectId = projectId;
		this.projectName = projectName;
		this.description = description;
		this.dueDate = dueDate;
		this.userList = userList;
		this.taskList = taskList;
		this.imgMod = imgMod;
	}


	

	public ProjectModel(String projectName, String description, LocalDate dueDate, List<UserModel> userList,
			List<TaskModel> taskList, ImageModel imgMod) {
		super();
		this.projectName = projectName;
		this.description = description;
		this.dueDate = dueDate;
		this.userList = userList;
		this.taskList = taskList;
		this.imgMod = imgMod;
	}




	public List<UserModel> getUserList() {
		return userList;
	}

	public void setUserList(List<UserModel> userList) {
		this.userList = userList;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

	public int getProjectId() {
		return projectId;
	}
	



	public List<TaskModel> getTaskList() {
		return taskList;
	}


	public void setTaskList(List<TaskModel> taskList) {
		this.taskList = taskList;
	}
	
	


	public ImageModel getImgMod() {
		return imgMod;
	}




	public void setImgMod(ImageModel imgMod) {
		this.imgMod = imgMod;
	}




	@Override
	public String toString() {
		return "ProjectModel [projectId=" + projectId + ", projectName=" + projectName + ", description=" + description
				+ ", dueDate=" + dueDate + ", userList=" + userList.get(0).getfName() + "]";
	}
	
	
	
	
	

}
