package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="priority")
public class PriorityModel {
	
	@Id
	@Column(name = "priority_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int prioityId;
	

	private String priorityString;
	
	public PriorityModel() {
		// TODO Auto-generated constructor stub
	}

	public PriorityModel(int prioityId, String priority) {
		super();
		this.prioityId = prioityId;
		this.priorityString = priority;
	}

	public PriorityModel(String priority) {
		super();
		this.priorityString = priority;
	}

	public String getPriority() {
		return priorityString;
	}

	public void setPriority(String priority) {
		this.priorityString = priority;
	}

	public int getPrioityId() {
		return prioityId;
	}

	@Override
	public String toString() {
		return "PriorityModel [prioityId=" + prioityId + ", priority=" + priorityString + "]";
	}
	
	

}
