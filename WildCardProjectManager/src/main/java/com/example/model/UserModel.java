package com.example.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.example.custom.ProjectCustom;
import com.example.custom.UserCustom;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@Entity
@Table(name="userTable")
@JsonSerialize(using = UserCustom.class)
public class UserModel {
	
	
	@Id
	@Column(name="user_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int userId;
	
	@Column(name="fName")
	private String fName;
	
	@Column(name = "lName")
	private String lName;
	
	@Column(name = "user_role")
	private int userRole;
	
	@Column(name ="username")
	private String username;
	
	@Column(name ="password")
	private String password;
	
	@ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@taskId")
	private List<TaskModel> tList;
	
	
	@ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@projectId")
	private List<ProjectModel> pmList;
	
	public UserModel() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public UserModel(String fName, String lName, int userRole, String username, String password, List<TaskModel> tList,
			List<ProjectModel> pmList) {
		super();
		this.fName = fName;
		this.lName = lName;
		this.userRole = userRole;
		this.username = username;
		this.password = password;
		this.tList = tList;
		this.pmList = pmList;
	}



	public UserModel(int userID, String fName, String lName, int userRole, String username, String password,
			List<TaskModel> tList, List<ProjectModel> pmList) {
		super();
		this.userId = userID;
		this.fName = fName;
		this.lName = lName;
		this.userRole = userRole;
		this.username = username;
		this.password = password;
		this.tList = tList;
		this.pmList = pmList;
	}



	public int getUserID() {
		return userId;
	}
	
	
	public String getfName() {
		return fName;
	}
	
	
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public int getUserRole() {
		return userRole;
	}
	public void setUserRole(int userRole) {
		this.userRole = userRole;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<TaskModel> gettList() {
		return tList;
	}
	public void settList(List<TaskModel> tList) {
		this.tList = tList;
	}
	public List<ProjectModel> getPmList() {
		return pmList;
	}
	public void setPmList(List<ProjectModel> pmList) {
		this.pmList = pmList;
	}
	@Override
	public String toString() {
		return "UserModel [userID=" + userId + ", fName=" + fName + ", lName=" + lName + ", userRole=" + userRole
				+ ", username=" + username + ", password=" + password + ", tList=" + tList.get(0).getTaskName() + ", pmList=" + pmList.get(0).getProjectName() + "]";
	}
	
	

}
