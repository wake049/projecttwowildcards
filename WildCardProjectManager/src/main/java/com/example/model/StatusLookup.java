package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;




import javax.persistence.Table;



@Entity
@Table(name="StatusLookup")

public class StatusLookup {
	
	@Id
	@Column(name ="statusId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int statusId;
	
	private String status;
	
	public StatusLookup() {
		// TODO Auto-generated constructor stub
	}

	public StatusLookup(String status) {
		super();
		this.status = status;
	}

	public StatusLookup(int statusId, String status) {
		super();
		this.statusId = statusId;
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStatusId() {
		return statusId;
	}

	@Override
	public String toString() {
		return "StatusLookup [statusId=" + statusId + ", status=" + status + "]";
	}
	
	
	
	
}
