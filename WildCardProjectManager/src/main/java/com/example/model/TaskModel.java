package com.example.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;



import javax.persistence.OneToOne;

import javax.persistence.Table;

import com.example.custom.TaskChangeCustom;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;





@Entity
@Table(name="Task")
@JsonSerialize(using = TaskChangeCustom.class)
public class TaskModel {
	
	
	@Id
	@Column(name="task_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int taskId;
	
	@Column(name="taskName")
	private String taskName;
	
	@Column(name ="description")
	private String description;
	
	@OneToOne(fetch = FetchType.EAGER)
	private StatusLookup statusTask;
	
	@Column(name = "tag")
	private String tag;
	
	@Column(name = "due_date")
	private LocalDate dueDate;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "priorityString")
	private PriorityModel priorModel;
	
	//private  image goes here
	
	
	@ManyToMany(cascade=CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@userId")
	private List<UserModel> userList;
	
	public TaskModel() {
		// TODO Auto-generated constructor stub
	}

	public TaskModel(String taskName, String description, StatusLookup statusTask, String tag, LocalDate dueDate,
			PriorityModel priorModel, List<UserModel> userList) {
		super();
		this.taskName = taskName;
		this.description = description;
		this.statusTask = statusTask;
		this.tag = tag;
		this.dueDate = dueDate;
		this.priorModel = priorModel;
		this.userList = userList;
	}

	public TaskModel(int taskId, String taskName, String description, StatusLookup statusTask, String tag,
			LocalDate dueDate, PriorityModel priorModel, List<UserModel> userList) {
		super();
		this.taskId = taskId;
		this.taskName = taskName;
		this.description = description;
		this.statusTask = statusTask;
		this.tag = tag;
		this.dueDate = dueDate;
		this.priorModel = priorModel;
		this.userList = userList;
	}


	public StatusLookup getStatusTask() {
		return statusTask;
	}

	public void setStatusTask(StatusLookup statusTask) {
		this.statusTask = statusTask;
	}

	public PriorityModel getPriorModel() {
		return priorModel;
	}


	public void setPriorModel(PriorityModel priorModel) {
		this.priorModel = priorModel;
	}


	public void setUserList(List<UserModel> userList) {
		this.userList = userList;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public StatusLookup getStatusId() {
		return statusTask;
	}

	public void setStatusId(StatusLookup statusTask) {
		this.statusTask = statusTask;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}



	public List<UserModel> getUserList() {
		return userList;
	}

	@Override
	public String toString() {
		return "TaskModel [taskId=" + taskId + ", taskName=" + taskName + ", description=" + description
				+ ", statusTask=" + statusTask + ", tag=" + tag + ", dueDate=" + dueDate + ", priorModel=" + priorModel
				+ ", userList=" + userList + "]";
	}
	
	
	
	
	

}
