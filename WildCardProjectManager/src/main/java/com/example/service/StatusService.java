package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.StatusLookup;
import com.example.repo.StatusRepo;

@Service
public class StatusService {
	
	private StatusRepo sRepo;
	
	public StatusService() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public StatusService(StatusRepo sRepo) {
		super();
		this.sRepo = sRepo;
	}
	
	public StatusLookup findByStatusS(String status) {
		return this.sRepo.findByStatus(status);
	}

	
}
