package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.UserModel;
import com.example.repo.UserRepo;

@Service
public class UserService {
	
	private UserRepo uRepo;
	
	public UserService () {
		
	}
	
	
	@Autowired
	public UserService(UserRepo uRepo) {
		super();
		this.uRepo = uRepo;
	}



	public UserModel findByUsername(String username) {
		// TODO Auto-generated method stub
		return this.uRepo.findByUsername(username);
	}

	public void insertUser(UserModel umodl) {
		// TODO Auto-generated method stub
		uRepo.save(umodl);
	}

	public List<UserModel> findAllUsers() {
		// TODO Auto-generated method stub
		return this.uRepo.findAll();
	}

	public void deleteUser(UserModel username) {
		// TODO Auto-generated method stub
		uRepo.delete(username);
	}
	
	public UserModel findByfName(String fname) {
		return this.uRepo.findByfName(fname);
	}
	

}
