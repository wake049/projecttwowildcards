package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.TaskModel;
import com.example.repo.TaskRepo;

@Service
public class TaskService {
	
	private TaskRepo tRepo;
	
	public TaskService () {
		
	}
	
	@Autowired
	public TaskService(TaskRepo tRepo) {
		super();
		this.tRepo = tRepo;
	}


	public List<TaskModel> getAllTasks() {
		// TODO Auto-generated method stub
		return this.tRepo.findAll();
	}

	public TaskModel getTasksByName(String taskName) {
		 List<TaskModel> tList = this.tRepo.findAll();
		 for(TaskModel task: tList) {
			 if(task.getTaskName().equals(taskName)) {
				 return task;
		}
	}
	return null;
	}


	public void insertTask(TaskModel tmodl) {
		// TODO Auto-generated method stub
		tRepo.save(tmodl);
	}

	public void deleteTask(TaskModel taskModel) {
		// TODO Auto-generated method stub
		tRepo.delete(taskModel);
	}


	
	
}
