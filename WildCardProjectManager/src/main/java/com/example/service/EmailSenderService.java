package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.example.model.UserModel;

@Service
public class EmailSenderService {

	JavaMailSender javaMailSender;
	private UserService uServ;
	
	
	
	@Autowired
	public EmailSenderService(JavaMailSender javaMailSender, UserService uServ) {
		super();
		this.javaMailSender = javaMailSender;
		this.uServ = uServ;
	}

	String password = null;
	
	public String sendEmail(String email) {
		SimpleMailMessage message = new SimpleMailMessage();
		UserModel tmp = uServ.findByUsername(email);
		password = tmp.getPassword();
		String newMessage = "Hello, "+ email + "."
				+"You have requested your password.\n"
				+"Here it is "+password +"\n"
				+ "Change password at localhost:4200/forgotpassword \n" 
				+ "or return to login at localhost:4200/login";
		message.setFrom("semailfrom@gmail.com");
		message.setTo(email);
		message.setSubject("Password Recovery");
		message.setText(newMessage);
		
		javaMailSender.send(message);
		
		return "Mail sent successfully";
	}
}