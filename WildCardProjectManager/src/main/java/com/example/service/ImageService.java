package com.example.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.util.StringUtils;
import com.example.model.ImageModel;
import com.example.repo.ImageRepo;


@Service
public class ImageService {
	
	private ImageRepo iRepo;
  
	public ImageService() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public ImageService(ImageRepo iRepo) {
		super();
		this.iRepo = iRepo;
	}
	
	public void save(MultipartFile file) throws IOException {
        ImageModel fileEntity = new ImageModel();
        fileEntity.setName(StringUtils.cleanPath(file.getOriginalFilename()));
        fileEntity.setContentType(file.getContentType());
        fileEntity.setData(file.getBytes());
        fileEntity.setSize(file.getSize());

        iRepo.save(fileEntity);
    }
	
    public List<ImageModel> getAllFiles() {
        return iRepo.findAll();
    }
}
