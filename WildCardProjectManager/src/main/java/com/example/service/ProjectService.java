package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.ProjectModel;
import com.example.model.UserModel;
import com.example.repo.ProjectRepo;

@Service
public class ProjectService {
	private ProjectRepo pRepo;
	
	public ProjectService() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public ProjectService(ProjectRepo pRepo) {
		super();
		this.pRepo = pRepo;
	}
	
	public List<ProjectModel> getAllProjects(){
		return pRepo.findAll();
	}

	public void insertProject(ProjectModel projmd) {
		// TODO Auto-generated method stub
		pRepo.save(projmd);
	}

	public ProjectModel getProjectByName(String projectName) {
		// TODO Auto-generated method stub
		return this.pRepo.findByProjectName(projectName);
	}

	public void deleteProject(ProjectModel projectModel) {
		// TODO Auto-generated method stub
		pRepo.delete(projectModel);
		
	}

	
	
}
