package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.PriorityModel;
import com.example.repo.PrioityRepo;

@Service
public class PriorityService {
	
	private PrioityRepo pRepo;
	
	public PriorityService() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public PriorityService(PrioityRepo pRepo) {
		super();
		this.pRepo = pRepo;
	}
	public PriorityModel findByPriority(String priorityString) {
		return this.pRepo.findBypriorityString(priorityString);
	}

}
