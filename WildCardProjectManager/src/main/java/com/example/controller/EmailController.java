package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.service.EmailSenderService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class EmailController {
		
	@Autowired
	EmailSenderService emailService;

	@GetMapping("/{username}")
	public ResponseEntity<String> sendEmail(@PathVariable("username") String username) {
		emailService.sendEmail(username);
		return ResponseEntity.status(201).body("Successfully Inserted");
	}
}

