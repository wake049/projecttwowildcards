package com.example.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.model.ProjectModel;
import com.example.model.UserModel;
import com.example.service.ProjectService;
import com.example.service.UserService;

@RestController
@RequestMapping(value = "/projectscreate")
@CrossOrigin(origins = "*")
public class ProjectCreate {

	private ProjectService pServ;
	private UserService uServ;
	private String Date;

	public ProjectCreate() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public ProjectCreate(ProjectService pServ, UserService uServ) {
		super();
		this.pServ = pServ;
		this.uServ = uServ;
	}

	@GetMapping
	public ResponseEntity<List<UserModel>> getAllUsers() {
		return ResponseEntity.status(200).body(uServ.findAllUsers());
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE) // Adding a new project
	public ResponseEntity<List<ProjectModel>> insertProject(@RequestBody Map<String, String> newMap) {
		List<UserModel> userList = new ArrayList<>();
		//Optional<ProjectModel> pm1 = Optional.ofNullable(pServ.getProjectByName(newMap.get("projectName")));
		String username = newMap.get("userFname");
		for (int i = 0; i < username.length(); i++) {
			if (!Character.isLetter(username.charAt(i))) {
				username = username.replace(username.charAt(i), ' ');
			}
		}
		String replace = username.replaceAll("\\p{Zs}+", " ");
		String trim = replace.trim();
		String newList[] = trim.split(" ");
		for (String string : newList) {
			UserModel um = uServ.findByfName(string);
			if (um != null)
				userList.add(um);
		}
		
		
		
		String projectname = newMap.get("project");
		String newprojectname1 = projectname.replaceAll(",", "");
		String newprojectname2 = newprojectname1.replaceAll("\"", "");
		String replace1 = newprojectname2.replaceAll("\\p{Zs}+", " ");
		String replace2 = replace1.replace("projectName", "");
		String replace3 = replace2.replace("description", "");
		String replace4 = replace3.replace("dueDate", "");
		String replace5 = replace3.replace("file", "");
		String newprojectname = replace5.substring(1,replace5.length()-1);
		String trim1 = newprojectname.trim();
		String newList1[] = trim1.split(":");
		for (String string : newList1) {
			System.out.println(string);
		}

		
		//System.out.println(pm1.get().getProjectName());
		//Optional<ProjectModel> pm1 = Optional.ofNullable(newList1[1]);
		//if (pm1.isPresent()) {
			System.out.println("gets here");
			DateTimeFormatter formatter =DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US);
			String date = newList1[3];
			System.out.println(date);
			LocalDate localDate = LocalDate.parse(date, formatter);
			ProjectModel pm = new ProjectModel();
			pm.setProjectName(newList1[1]);
			System.out.println(pm.getProjectName());
			pm.setDueDate(localDate);
			pm.setDescription(newList1[2]);
			pm.setUserList(userList);
			System.out.println(pm.getDescription());
			if(pm != null) {
				pServ.insertProject(pm);
			}
			List<ProjectModel> pmList = new ArrayList<>();
			for (String string : newList) {
				UserModel um = uServ.findByfName(string);
				if (um != null)
					pmList = um.getPmList();
					pmList.add(pm);
					um.setPmList(pmList);
					uServ.insertUser(um);
					
			}
			return ResponseEntity.status(201).body(pServ.getAllProjects());
		//}

		//return ResponseEntity.noContent().build();
	}
}
