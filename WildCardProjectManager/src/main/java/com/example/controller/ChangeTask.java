package com.example.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.PriorityModel;
import com.example.model.StatusLookup;
import com.example.model.TaskModel;
import com.example.model.UserModel;
import com.example.repo.StatusRepo;
import com.example.service.PriorityService;
import com.example.service.ProjectService;
import com.example.service.StatusService;
import com.example.service.TaskService;
import com.example.service.UserService;

@RestController
@CrossOrigin(origins = "*")
public class ChangeTask {

	private ProjectService pServ;
	private TaskService tServ;
	private UserService uServ;
	private TaskModel tmodl;
	private StatusService sServ;
	private PriorityService prioServ;

	public ChangeTask() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public ChangeTask(ProjectService pServ, UserService uServ, TaskService tServ,StatusService sServ, PriorityService prioServ) {
		super();
		this.pServ = pServ;
		this.uServ = uServ;
		this.tServ = tServ;
		this.sServ = sServ;
		this.prioServ = prioServ;
	}
	@PostMapping(value = "/taskcreate/change/new")
	public ResponseEntity<String> insertChanges(@RequestBody Map<String, String> newMap) {
		List<UserModel> userList = new ArrayList<>();
		String username = newMap.get("name");
		for (int i = 0; i < username.length(); i++) {
			if (!Character.isLetter(username.charAt(i))) {
				username = username.replace(username.charAt(i), ' ');
			}
		}
		String replace = username.replaceAll("\\p{Zs}+", " ");
		String trim = replace.trim();
		String newList[] = trim.split(" ");
		for (String string : newList) {
			UserModel um = uServ.findByfName(string);
			if (um != null)
				userList.add(um);
		}
		String projectname = newMap.get("user");
		String newprojectname1 = projectname.replaceAll(",", "");
		String newprojectname2 = newprojectname1.replaceAll("\"", "");
		String replace1 = newprojectname2.replaceAll("\\p{Zs}+", " ");
		String replace2 = replace1.replace("Status", "");
		String replace3 = replace2.replace("tag", "");
		String replace4 = replace3.replace("Priority", "");
		String newprojectname = replace4.substring(1,replace4.length()-1);
		String trim1 = newprojectname.trim();
		String newList1[] = trim1.split(":");
		for (String string : newList1) {
			System.out.println(string);
		}
		
		tmodl.setStatusTask(sServ.findByStatusS(newList1[1]));
		tmodl.setPriorModel(prioServ.findByPriority(newList1[3]));
		tmodl.setTag(newList1[2]);
		tmodl.setUserList(userList);
		tServ.insertTask(tmodl);
		
		
		return ResponseEntity.status(200).body("Successfully Inserted");

	}

	@GetMapping(value = "/taskcreate/{taskName}")
	public ResponseEntity<TaskModel> getTaskByName(@PathVariable("taskName") String taskName) {
		tmodl = tServ.getTasksByName(taskName);
		return ResponseEntity.status(200).body(tmodl);

	}

	@GetMapping(value = "/taskcreate")
	public ResponseEntity<List<UserModel>> getAllUsers() {
		return ResponseEntity.status(200).body(uServ.findAllUsers());
	}


	/*
	 * @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE) // Adding a new
	 * project public ResponseEntity<List<ProjectModel>> insertProject(@RequestBody
	 * Map<String, String> newMap) { List<UserModel> userList = new ArrayList<>();
	 * //Optional<ProjectModel> pm1 =
	 * Optional.ofNullable(pServ.getProjectByName(newMap.get("projectName")));
	 * String username = newMap.get("userFname"); for (int i = 0; i <
	 * username.length(); i++) { if (!Character.isLetter(username.charAt(i))) {
	 * username = username.replace(username.charAt(i), ' '); } } String replace =
	 * username.replaceAll("\\p{Zs}+", " "); String trim = replace.trim(); String
	 * newList[] = trim.split(" "); for (String string : newList) { UserModel um =
	 * uServ.findByfName(string); if (um != null) userList.add(um); }
	 * 
	 * 
	 * 
	 * String projectname = newMap.get("project"); String newprojectname1 =
	 * projectname.replaceAll(",", ""); String newprojectname2 =
	 * newprojectname1.replaceAll("\"", ""); String replace1 =
	 * newprojectname2.replaceAll("\\p{Zs}+", " "); String replace2 =
	 * replace1.replace("projectName", ""); String replace3 =
	 * replace2.replace("description", ""); String replace4 =
	 * replace3.replace("dueDate", ""); String newprojectname =
	 * replace4.substring(1,replace4.length()-1); String trim1 =
	 * newprojectname.trim(); String newList1[] = trim1.split(":"); for (String
	 * string : newList1) { System.out.println(string); }
	 * 
	 * 
	 * //System.out.println(pm1.get().getProjectName()); //Optional<ProjectModel>
	 * pm1 = Optional.ofNullable(newList1[1]); //if (pm1.isPresent()) {
	 * System.out.println("gets here"); DateTimeFormatter formatter
	 * =DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US); String date =
	 * newList1[3]; System.out.println(date); LocalDate localDate =
	 * LocalDate.parse(date, formatter); ProjectModel pm = new ProjectModel();
	 * pm.setProjectName(newList1[1]); System.out.println(pm.getProjectName());
	 * pm.setDueDate(localDate); pm.setDescription(newList1[2]);
	 * pm.setUserList(userList); System.out.println(pm.getDescription()); if(pm !=
	 * null) { pServ.insertProject(pm); } return
	 * ResponseEntity.status(201).body(pServ.getAllProjects()); //}
	 * 
	 * //return ResponseEntity.noContent().build(); }
	 */
}
