package com.example.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.ProjectModel;
import com.example.model.TaskModel;
import com.example.service.PriorityService;
import com.example.service.ProjectService;
import com.example.service.StatusService;
import com.example.service.TaskService;

@RestController
@RequestMapping(value="/create")
@CrossOrigin(origins="*")
public class CreateTaskController {


		
		private TaskService tServ;
		private ProjectService pServ;
		private StatusService sServ;
		private PriorityService prioServ;
		public CreateTaskController() {
			
		}
		@Autowired
		public CreateTaskController(TaskService tServ, ProjectService pServ, StatusService sServ, PriorityService prioServ) {
			super();
			this.tServ = tServ;
			this.pServ = pServ;
			this.sServ = sServ;
			this.prioServ = prioServ;
		}
		
		@GetMapping
		public ResponseEntity<List<ProjectModel>> getAllProjects() {
			return ResponseEntity.status(200).body(pServ.getAllProjects());
			
		}
		
		@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE) //  Adding a task
		public ResponseEntity<List<TaskModel>> insertTask(@RequestBody Map<String, String> newMap) {
			TaskModel tmodl = new TaskModel();
			ProjectModel pm = new ProjectModel();
			tmodl.setStatusTask(sServ.findByStatusS((newMap.get("statusTask"))));
			tmodl.setPriorModel(prioServ.findByPriority(newMap.get("priorModel")));
			tmodl.setTaskName(newMap.get("taskName"));
			tmodl.setDescription(newMap.get("description"));
			tmodl.setTag(newMap.get("tag"));
			DateTimeFormatter formatter =DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US);
			String date = newMap.get("dueDate");
			LocalDate localDate = LocalDate.parse(date, formatter);
			tmodl.setDueDate(localDate);
			tServ.insertTask(tmodl);
			pm = pServ.getProjectByName(newMap.get("projectName"));
			List<TaskModel> tmList = new ArrayList<>();
			tmList = (pm.getTaskList());
			tmList.add(tmodl);
			pm.setTaskList(tmList);
			pServ.insertProject(pm);
			return  ResponseEntity.status(201).body(tServ.getAllTasks());
		}
		
		
}
