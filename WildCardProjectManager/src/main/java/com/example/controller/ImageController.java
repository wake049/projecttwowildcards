package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.service.ImageService;

@RestController
@RequestMapping(value = "/files")
@CrossOrigin(origins = "*")
public class ImageController {
	 private final ImageService iServ;

	    @Autowired
	    public ImageController(ImageService iServ) {
	        this.iServ = iServ;
	    }

	    @PostMapping
	    public ResponseEntity<String> upload(@RequestParam("file") MultipartFile file) {
	    	System.out.println("called");
	        try {
	        	iServ.save(file);

	            return ResponseEntity.status(HttpStatus.OK)
	                                 .body(String.format("File uploaded successfully: %s", file.getOriginalFilename()));
	        } catch (Exception e) {
	            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	                                 .body(String.format("Could not upload the file: %s!", file.getOriginalFilename()));
	        }
	    }
}
