package com.example.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.ProjectModel;
import com.example.model.UserModel;
import com.example.service.ProjectService;
import com.example.service.UserService;




@RestController
@RequestMapping(value="/projects")
@CrossOrigin(origins="*")
public class ProjectController {
	
	private ProjectService pServ;
	private UserService uServ;
	
	public ProjectController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public ProjectController(ProjectService pServ,  UserService uServ) {
		super();
		this.pServ = pServ;
		this.uServ = uServ;
	}
	
	@CrossOrigin(origins="*")
	@GetMapping
	public ResponseEntity<List<ProjectModel>> getAllProjectsByUsername(){
		UserModel um = uServ.findByUsername(LoginController.loginUser.getUsername());
		return ResponseEntity.status(200).body(um.getPmList());
	}
	
	@CrossOrigin(origins="*")
	@GetMapping("/{projectName}")
	public ResponseEntity<ProjectModel> getUserByUserName(@PathVariable("projectName") String projectName) {
		System.out.println(projectName);
		Optional<ProjectModel> userOpt = Optional.ofNullable(pServ.getProjectByName(projectName));
		if (userOpt.isPresent()) {
			return ResponseEntity.status(200).body(userOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
	@CrossOrigin(origins="*")
	@DeleteMapping("/{projectname}")
	public ResponseEntity<ProjectModel> deleteProject(@PathVariable("projectname") String projectName){
		Optional<ProjectModel> projectOpt = Optional.ofNullable(pServ.getProjectByName(projectName));
		if(projectOpt.isPresent()) {
			pServ.deleteProject(projectOpt.get());
			return ResponseEntity.status(200).body(projectOpt.get());
		}
		return ResponseEntity.notFound().build();
		
	}
}
