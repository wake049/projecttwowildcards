package com.example.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.TaskModel;
import com.example.service.TaskService;
import com.example.service.UserService;

@RestController
@RequestMapping(value="/task")
@CrossOrigin(origins="*")
public class TaskController {

	
	private TaskService tServ;
	private UserService uServ;
	public TaskController() {
		
	}
	@Autowired
	public TaskController(TaskService tServ, UserService uServ) {
		super();
		this.tServ = tServ;
		this.uServ = uServ;
	
		
	}

	@CrossOrigin(origins="*")
	@GetMapping
	public @ResponseBody List<TaskModel> getAllTasks() {
		return LoginController.loginUser.gettList();
	}
	
	@GetMapping(value="/{taskName}") 
	public ResponseEntity<TaskModel> getTaskByName(@PathVariable("taskName") String taskName) {
		TaskModel tmodl = tServ.getTasksByName(taskName);
		System.out.println(tmodl);
		return ResponseEntity.status(200).body(tmodl);
		
		
	}
		
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE) //  Adding a task
	public ResponseEntity<List<TaskModel>> insertTask(@RequestBody TaskModel tmodl) {
		System.out.println(tmodl);
		if(tServ.getTasksByName(tmodl.getTaskName())==null) {
			tServ.insertTask(tmodl);
			return  ResponseEntity.status(201).body(tServ.getAllTasks());
		}
		return  ResponseEntity.noContent().build();
	}
	
	
	@DeleteMapping("/{taskName}")
	public ResponseEntity<TaskModel> deleteTask(@PathVariable("taskName") String taskName) {
		Optional<TaskModel> taskOpt = Optional.ofNullable(tServ.getTasksByName(taskName));
		if (taskOpt.isPresent()) {
			tServ.deleteTask(taskOpt.get());
			return ResponseEntity.status(200).body(taskOpt.get());
		}
		return ResponseEntity.notFound().build();
	}

}
