package com.example.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.ImageModel;
import com.example.model.PriorityModel;
import com.example.model.ProjectModel;
import com.example.model.StatusLookup;
import com.example.model.TaskModel;
import com.example.model.UserModel;
import com.example.repo.ImageRepo;
import com.example.repo.PrioityRepo;
import com.example.repo.ProjectRepo;
import com.example.repo.StatusRepo;
import com.example.repo.TaskRepo;
import com.example.repo.UserRepo;

@RestController
public class ORMController {

	public UserRepo uRepo;
	public TaskRepo tRepo;
	public ProjectRepo pRepo;
	public PrioityRepo prRepo;
	public StatusRepo sRepo;
	public ImageRepo imgRepo;

	
	public ORMController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public ORMController(UserRepo uRepo, TaskRepo tRepo, ProjectRepo pRepo, PrioityRepo prRepo, StatusRepo sRepo,
			ImageRepo imgRepo) {
		super();
		this.uRepo = uRepo;
		this.tRepo = tRepo;
		this.pRepo = pRepo;
		this.prRepo = prRepo;
		this.sRepo = sRepo;
		this.imgRepo = imgRepo;
	}
	
	@GetMapping(value = "/init")
	public ResponseEntity<String> insertInitialValues() {
		System.out.println("init called");
		// Status Creation
		StatusLookup sm1 = new StatusLookup("Pending");
		StatusLookup sm2 = new StatusLookup("Completed");
		StatusLookup sm3 = new StatusLookup("Working on");

		// Priority Mode Creation
		PriorityModel priorm1 = new PriorityModel("High");
		PriorityModel priorm2 = new PriorityModel("Medium");
		PriorityModel priorm3 = new PriorityModel("Low");

		// User Creation
		UserModel um1 = new UserModel("William", "Allen", 1, "wakebrdtyler@yahoo.com", "S706981234",
				new ArrayList<TaskModel>(), new ArrayList<ProjectModel>());
		UserModel um2 = new UserModel("KamAu", "Amen", 1, "kamau.amen@revature.net", "12345",
				new ArrayList<TaskModel>(), new ArrayList<ProjectModel>());
		UserModel um3 = new UserModel("Christopher", "Mendy", 1, "Christopher.mendy@revature.net", "12345",
				new ArrayList<TaskModel>(), new ArrayList<ProjectModel>());
		UserModel um4 = new UserModel("Suman", "Singh", 1, "Suman.singh@revature.net", "12345",
				new ArrayList<TaskModel>(), new ArrayList<ProjectModel>());
		UserModel um5 = new UserModel("Tracy", "Bodine", 2, "akljdsghflk@ajkhdf.net", "12345",
				new ArrayList<TaskModel>(), new ArrayList<ProjectModel>());
		UserModel um6 = new UserModel("Andre", "B", 2, "akljdsflk@ajkhdf.net", "12345", 
				new ArrayList<TaskModel>(), new ArrayList<ProjectModel>());

		// Task Creation for Three Projects

		List<UserModel> useList = new ArrayList<>();
				useList.addAll(Arrays.asList(um1, um2, um3, um4, um5, um6));
		TaskModel tm1 = new TaskModel("Get all clients", "Grabs all the clients in a database", sm1, "java",
				LocalDate.now(), priorm2, useList);
		TaskModel tm2 = new TaskModel("Creates a new client", "Creates a new Client and adds them to the database", sm2,
				"java", LocalDate.now(), priorm3, useList);
		TaskModel tm3 = new TaskModel("Get a client", "Grabs the client with id of 10", sm2, "java", LocalDate.now(),
				priorm1, useList);
		TaskModel tm4 = new TaskModel("Updates client", "Changes specfic client information depending on the id", sm2,
				"java", LocalDate.now(), priorm1, useList);
		TaskModel tm5 = new TaskModel("delete client", "Delete a client at a certain id", sm2, "java", LocalDate.now(),
				priorm1, useList);
		TaskModel tm6 = new TaskModel("Create a new account", "creates a new account for a specific id", sm2, "java",
				LocalDate.now(), priorm1, useList);
		TaskModel tm7 = new TaskModel("Get all accounts for client",
				"Client should be able to see all there specific accounts", sm2, "java", LocalDate.now(), priorm1,
				useList);
		TaskModel tm8 = new TaskModel("Update account", "Update account type", sm2, "java", LocalDate.now(), priorm1,
				useList);
		TaskModel tm9 = new TaskModel("Delete account", "Delete a account at an id", sm2, "java", LocalDate.now(),
				priorm1, useList);
		TaskModel tm10 = new TaskModel("deposit", "Account balance goes up", sm2, "java", LocalDate.now(), priorm1,
				useList);
		TaskModel tm11 = new TaskModel("withdraw", "Account balance goes down", sm2, "java", LocalDate.now(), priorm1,
				useList);
		TaskModel tm12 = new TaskModel("get all accounts between two points", "Get all accounts if between two numbers",
				sm2, "java", LocalDate.now(), priorm1, useList);
		List<TaskModel> tList = new ArrayList<>();
				tList.addAll(Arrays.asList(tm1, tm2, tm3, tm4, tm5, tm6, tm7, tm8, tm9, tm10, tm11, tm12));
		ProjectModel pm1 = new ProjectModel("Project0", "Create a Banking API using Javalin", LocalDate.now(), useList,
				tList, new ImageModel());

		useList = new ArrayList<>();
		useList.addAll(Arrays.asList(um1, um2, um3, um4, um6));
		TaskModel tm13 = new TaskModel("Employee view", "Employee can view all reimbursments", sm2, "html",
				LocalDate.now(), priorm1, useList);
		TaskModel tm14 = new TaskModel("Employee Create", "Employee can create/submit new reimbursments", sm2, "html",
				LocalDate.now(), priorm1, useList);
		TaskModel tm15 = new TaskModel("Manager view", "Manager should be able to see all reimbursements", sm2, "html",
				LocalDate.now(), priorm1, useList);
		TaskModel tm16 = new TaskModel("Manager Change", "Manager can change the status of all pending reimbursements",
				sm2, "html", LocalDate.now(), priorm1, useList);
		TaskModel tm17 = new TaskModel("Manager Stats", "Manager can view a statistics page", sm2, "html",
				LocalDate.now(), priorm1, useList);
		tList = new ArrayList<>();
		tList.addAll(Arrays.asList(tm13, tm14, tm15, tm16, tm17));
		ProjectModel pm2 = new ProjectModel("Project1",
				"Create a reimbursment website were employees can create reimbursements and managers can approve reimbursements",
				LocalDate.now(), useList, tList, new ImageModel());

		useList = new ArrayList<>();
		useList.addAll(Arrays.asList(um1, um2, um3, um4));
		TaskModel tm18 = new TaskModel("Employee view", "Employee can view all projects and task they are assigned to",
				sm3, "html", LocalDate.now(), priorm1, useList);
		TaskModel tm19 = new TaskModel("Employee Change", "Employee can change the status of a task", sm3, "html",
				LocalDate.now(), priorm1, useList);
		TaskModel tm20 = new TaskModel("Manager view",
				"Manager should be able to see all tasks, projects, and employees", sm3, "html", LocalDate.now(),
				priorm1, useList);
		TaskModel tm21 = new TaskModel("Manager Create",
				"Manager can create projects and tasks and assign users to a task", sm3, "html", LocalDate.now(),
				priorm1, useList);
		TaskModel tm22 = new TaskModel("Manager Due", "Manager can set a due date", sm3, "html", LocalDate.now(),
				priorm1, useList);
		TaskModel tm23 = new TaskModel("Manager status", "Manager can set status", sm3, "html", LocalDate.now(),
				priorm1, useList);
		TaskModel tm24 = new TaskModel("Manager", "Manager can  a due date", sm3, "html", LocalDate.now(), priorm1,
				useList);
		tList = new ArrayList<>();
		tList.addAll(Arrays.asList(tm18, tm19, tm20, tm21, tm22, tm23, tm24));
		ProjectModel pm3 = new ProjectModel("Project2", "Create a Project Management Tool Using Spring and Angular",
				LocalDate.now(), useList, tList, new ImageModel());

		List<TaskModel> tListAll = new ArrayList<>();
		tListAll.addAll(Arrays.asList(tm1, tm2, tm3, tm4, tm5, tm6, tm7, tm8, tm9, tm10, tm11, tm12, tm13, tm14, tm15,
				tm16, tm17, tm18, tm19, tm20, tm21, tm22, tm23, tm24));

		this.prRepo.save(priorm1);
		this.prRepo.save(priorm2);
		this.prRepo.save(priorm3);

		this.sRepo.save(sm1);
		this.sRepo.save(sm2);
		this.sRepo.save(sm3);
		for (UserModel useModel : useList) {
			this.uRepo.save(useModel);
		}

		for (TaskModel taskModel : tListAll) {
			this.tRepo.save(taskModel);
		}
		this.pRepo.save(pm1);
		this.pRepo.save(pm2);
		this.pRepo.save(pm3);
		/*
		 * this.prRepo.save(priorm1);
		this.prRepo.save(priorm2);
		this.prRepo.save(priorm3);
		
		this.sRepo.save(sm1);
		this.sRepo.save(sm2);
		this.sRepo.save(sm3);
		for (UserModel useModel : useList) {
			this.uRepo.save(useModel);
		}
		
		
		for (TaskModel taskModel : tListAll) {
			this.tRepo.save(taskModel);
		}
		this.pRepo.save(pm1);
		this.pRepo.save(pm2);
		this.pRepo.save(pm3);

		 * */

		/*
		 * List<ProjectModel> pmList = new ArrayList<>();
		 * pmList.addAll(Arrays.asList(pm1, pm2,pm3)); UserModel newum1P = um1;
		 * newum1P.setPmList(pmList); UserModel newum2P = um2;
		 * newum2P.setPmList(pmList); UserModel newum3P = um3;
		 * newum3P.setPmList(pmList); UserModel newum4P = um4;
		 * newum4P.setPmList(pmList); pmList = new ArrayList<>();
		 * pmList.addAll(Arrays.asList(pm1)); UserModel newum5P = um5;
		 * newum5P.setPmList(pmList); pmList = new ArrayList<>();
		 * pmList.addAll(Arrays.asList(pm1, pm2)); UserModel newum6P = um6;
		 * newum6P.setPmList(pmList);
		 * 
		 * List<UserModel> newUsList = new ArrayList<>(pm1.getUserList());
		 * //System.out.println(newUsList); for (UserModel userModel : newUsList) {
		 * this.uRepo.save(userModel); }
		 */

		return ResponseEntity.status(201).body("Successfully Inserted");

	}


	@GetMapping(value = "/update")
	public ResponseEntity<String> newGet() {
		System.out.println("update");
		UserModel um = this.uRepo.findByUsername("wakebrdtyler@yahoo.com");
		List<ProjectModel> pList = this.pRepo.findAll();
		um.setPmList(pList);
		List<TaskModel> tList = new ArrayList<>();
		for (ProjectModel projectModel : pList) {
			tList.addAll(projectModel.getTaskList());
		}
		um.settList(tList);
		
		
		UserModel um1 = this.uRepo.findByUsername("kamau.amen@revature.net");
		List<ProjectModel> pList1 = this.pRepo.findAll();
		um1.setPmList(pList1);
		List<TaskModel> tList1 = new ArrayList<>();
		for (ProjectModel projectModel : pList1) {
			tList1.addAll(projectModel.getTaskList());
		}
		um1.settList(tList);

		UserModel um2 = this.uRepo.findByUsername("Christopher.mendy@revature.net");
		List<ProjectModel> pList2 = this.pRepo.findAll();
		um2.setPmList(pList2);
		List<TaskModel> tList2 = new ArrayList<>();
		for (ProjectModel projectModel : pList2) {
			tList2.addAll(projectModel.getTaskList());
		}
		um2.settList(tList);

		UserModel um3 = this.uRepo.findByUsername("Suman.singh@revature.net");
		List<ProjectModel> pList3 = this.pRepo.findAll();
		um3.setPmList(pList3);
		List<TaskModel> tList3 = new ArrayList<>();
		for (ProjectModel projectModel : pList3) {
			tList3.addAll(projectModel.getTaskList());
		}
		um3.settList(tList);

		this.uRepo.save(um);
		this.uRepo.save(um1);
		this.uRepo.save(um2);
		this.uRepo.save(um3);
		

		return ResponseEntity.status(201).body("Successfully Inserted");

	}
	@GetMapping(value = "/project0")
	public ResponseEntity<String> projectZero() {
		UserModel um4 = this.uRepo.findByUsername("akljdsghflk@ajkhdf.net");
		ProjectModel pm = this.pRepo.findByProjectName("Project0");
		List<ProjectModel> pList4 = new ArrayList<>();
		if(pm != null)
		pList4.addAll(Arrays.asList(pm));
		um4.setPmList(pList4);
		List<TaskModel> tList4 = new ArrayList<>();
		for (ProjectModel projectModel : pList4) {
			tList4.addAll(projectModel.getTaskList());
		}
		um4.settList(tList4);
		this.uRepo.save(um4);
		return ResponseEntity.status(201).body("Successfully Inserted");
	}
	@GetMapping(value = "/project1")
	public ResponseEntity<String> projecttwo() {
		UserModel um5 = this.uRepo.findByUsername("akljdsflk@ajkhdf.net");
		List<ProjectModel> pList5 = new ArrayList<>();
		ProjectModel pm1 = this.pRepo.findByProjectName("Project0");
		ProjectModel pm2 = this.pRepo.findByProjectName("Project1");
		if(pm2 != null && pm1 != null)
		pList5.addAll(Arrays.asList(pm1, pm2));
		um5.setPmList(pList5);
		List<TaskModel> tList5 = new ArrayList<>();
		for (ProjectModel projectModel : pList5) {
			tList5.addAll(projectModel.getTaskList());
		}
		um5.settList(tList5);
		this.uRepo.save(um5);
		return ResponseEntity.status(201).body("Successfully Inserted");
	}
	@GetMapping(value ="/fix")
	public ResponseEntity<String> findByStatus(){
		ProjectModel pm = pRepo.findByProjectName("Project0");
		pm.setImgMod(imgRepo.findImageByName("Bankingapi.png"));
		this.pRepo.save(pm);
		return ResponseEntity.status(201).body("Successfully Inserted");
	}
	
}

