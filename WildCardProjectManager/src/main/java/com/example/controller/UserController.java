package com.example.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.example.model.UserModel;
import com.example.service.UserService;

@RestController
@RequestMapping(value = "/user")
@CrossOrigin(origins = "*")
public class UserController {

	private String username;
	private String password;

	private UserService uServ;

	public UserController() {

	}
	

	@Autowired
	public UserController(UserService uServ) {
		super();
		this.uServ = uServ;
	}

	@GetMapping()
	public ResponseEntity<List<UserModel>> getAllUsers() {
		return ResponseEntity.status(200).body(uServ.findAllUsers());
	}

	@CrossOrigin(origins = "*")
	@GetMapping("/{username}")
	public ResponseEntity<UserModel> getUserByUserName(@PathVariable("username") String username) {
		Optional<UserModel> userOpt = Optional.ofNullable(uServ.findByUsername(username));
		if (userOpt.isPresent()) {
			return ResponseEntity.status(200).body(userOpt.get());
		}
		return ResponseEntity.notFound().build();
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE) // Adding a user
	public ResponseEntity<UserModel> insertUser(@RequestBody Map<String, String> newMap) {
		Optional<UserModel> foodOpt = Optional.ofNullable(uServ.findByUsername(newMap.get("username")));
		if(foodOpt.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		UserModel um = new UserModel();
		um.setfName(newMap.get("fname"));
		um.setlName(newMap.get("lname"));
		um.setUsername(newMap.get("username"));
		um.setPassword(newMap.get("password"));
		um.setUserRole(2);
		uServ.insertUser(um);
		return ResponseEntity.status(201).body(uServ.findByUsername(newMap.get("username")));

	}
	
	//@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE) //  Adding a user
	//public ResponseEntity<List<UserModel>> insertUser(@RequestBody UserModel umodl) {
		//System.out.println(umodl);
		//if(uServ.findByUsername(umodl.getUsername())==null) {
			//uServ.insertUser(umodl);
			//return  ResponseEntity.status(201).body(uServ.findAllUsers());
		//}
		//return  ResponseEntity.noContent().build();
		
	//}
	
	@GetMapping("/register")
	public String showSignUoForm(Model model) {
		model.addAttribute("user", new UserModel());
		return "signup_form";
	}
	

	@DeleteMapping("/{username}")
	public ResponseEntity<UserModel> deleteUser(@PathVariable("username") String username) {
		Optional<UserModel> userOpt = Optional.ofNullable(uServ.findByUsername(username));
		if (userOpt.isPresent()) {
			uServ.deleteUser(userOpt.get());
			return ResponseEntity.status(200).body(userOpt.get());
		}
		return ResponseEntity.notFound().build();

	}

}
