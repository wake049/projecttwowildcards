package com.example.controller;

import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.UserModel;
import com.example.repo.UserRepo;
import com.example.service.UserService;

@RestController
@RequestMapping(value = "/login")
@CrossOrigin(origins = "*")
public class LoginController {

	private String username;
	private String password;

	private UserService uServ;
	private UserRepo uRepo;
	
	public static UserModel loginUser;

	public LoginController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public LoginController(UserService uServ, UserRepo uRepo) {
		super();
		this.uServ = uServ;
		this.uRepo = uRepo;
	}
	

	@CrossOrigin(origins = "*")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserModel> verifyLogin(@RequestBody Map<String, String> newMap) {
		username = newMap.get("username");
		password = newMap.get("password");
		System.out.println("username");
		UserModel user = uServ.findByUsername(username);
		if(user != null)
			if (newMap.get("username").equals(user.getUsername()) && newMap.get("password").equals(user.getPassword())) {
				loginUser =user;
				return ResponseEntity.status(201).body(user);
		}
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping(value="/change", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserModel> changePassword(@RequestBody Map<String, String> newMap) {
		username = newMap.get("username");
		password = newMap.get("password");
		UserModel user = uServ.findByUsername(username);
		System.out.println(user);
		if(user != null) {
			user.setPassword(password);
			this.uRepo.save(user);
			return ResponseEntity.status(201).body(user);
		}
		return ResponseEntity.notFound().build();

	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("/{username}")
	public ResponseEntity<UserModel> getUserByUserName(@PathVariable("username") String username) {
		Optional<UserModel> userOpt = Optional.ofNullable(uServ.findByUsername(username));
		if (userOpt.isPresent()) {
			return ResponseEntity.status(200).body(userOpt.get());
		}
		return ResponseEntity.notFound().build();
	}
}

