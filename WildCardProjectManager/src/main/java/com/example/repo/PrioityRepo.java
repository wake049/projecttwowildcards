package com.example.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.PriorityModel;

public interface PrioityRepo extends JpaRepository<PriorityModel, Integer> {
	public PriorityModel findBypriorityString(String priorityString);
}
