package com.example.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.ProjectModel;

public interface ProjectRepo extends JpaRepository<ProjectModel, Integer> {

	public List<ProjectModel> findAll();
	public ProjectModel findByProjectName(String projectName);
	
}
