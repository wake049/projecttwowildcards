package com.example.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.ImageModel;

public interface ImageRepo extends JpaRepository<ImageModel, Integer> {

	public ImageModel findImageByName(String name);
	
	
}
