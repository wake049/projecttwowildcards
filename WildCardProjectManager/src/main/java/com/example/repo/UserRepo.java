package com.example.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.UserModel;

public interface UserRepo extends JpaRepository<UserModel, Integer> {
	
	public UserModel findByUsername(String username);
	public List<UserModel> findAll();
	public UserModel findByfName(String fName);
}
