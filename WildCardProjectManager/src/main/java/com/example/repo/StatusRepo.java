package com.example.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.StatusLookup;
import com.example.model.UserModel;

public interface StatusRepo extends JpaRepository<StatusLookup, Integer> {
	public StatusLookup findByStatus(String status);
	
}
