package com.example.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.TaskModel;

public interface TaskRepo extends JpaRepository<TaskModel, Integer> {
	
	public List<TaskModel> findAll();
	public TaskModel findTasksBytaskName(String taskName);
	
}
