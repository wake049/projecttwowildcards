package com.example.custom;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.example.model.TaskModel;
import com.example.model.UserModel;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class TaskChangeCustom extends StdSerializer<TaskModel> {

	public TaskChangeCustom() {
		super(TaskModel.class);
	}

	@Override
	public void serialize(TaskModel value, JsonGenerator gen, SerializerProvider provider) throws IOException {

		gen.writeStartObject();
		gen.writeNumberField("taskId", value.getTaskId());
		gen.writeStringField("taskName", value.getTaskName());
		gen.writeStringField("description", value.getDescription());
		gen.writeStringField("tag", value.getTag());
		gen.writeStringField("statusTask", value.getStatusTask().getStatus());
		gen.writeStringField("priority", value.getPriorModel().getPriority());
		gen.writeObjectField("dueDate", value.getDueDate());
		gen.writeArrayFieldStart("useList");
		if (value.getUserList() != null) {
			for (UserModel use : value.getUserList()) {
				gen.writeStartObject();
				gen.writeStringField("fName", use.getfName());
				gen.writeEndObject();
			}
		}
		gen.writeEndArray();
		gen.writeEndObject();
	}

}
