package com.example.custom;

import java.io.IOException;

import com.example.model.ProjectModel;
import com.example.model.TaskModel;
import com.example.model.UserModel;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class ProjectCustom extends StdSerializer<ProjectModel> {

	protected ProjectCustom() {
		super(ProjectModel.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void serialize(ProjectModel value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeStartObject();
		gen.writeNumberField("projectId", value.getProjectId());
		gen.writeStringField("projectName", value.getProjectName());
		gen.writeStringField("description", value.getDescription());
		if(value.getImgMod() != null)
		gen.writeObjectField("imgName", value.getImgMod());
		gen.writeObjectField("dueDate", value.getDueDate());
		gen.writeArrayFieldStart("useList");
		if (value.getUserList() != null) {
			for (UserModel use : value.getUserList()) {
				gen.writeStartObject();
				gen.writeStringField("fName", use.getfName());
				gen.writeEndObject();
			}
			gen.writeEndArray();
		}
		if (value.getTaskList() != null) {
			gen.writeArrayFieldStart("taskList");
			for (TaskModel use : value.getTaskList()) {
				gen.writeStartObject();
				gen.writeNumberField("taskId", use.getTaskId());
				gen.writeStringField("taskName", use.getTaskName());
				gen.writeStringField("description", use.getDescription());
				gen.writeStringField("tag", use.getTag());
				gen.writeStringField("statusTask", use.getStatusTask().getStatus());
				gen.writeStringField("priority", use.getPriorModel().getPriority());
				gen.writeObjectField("dueDate", use.getDueDate());
				gen.writeEndObject();
			}
			gen.writeEndArray();
		}
		gen.writeEndObject();

	}

}
