package com.example.custom;

import java.io.IOException;

import com.example.model.ProjectModel;
import com.example.model.TaskModel;
import com.example.model.UserModel;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class UserCustom extends StdSerializer<UserModel> {

	public UserCustom() {
		super(UserModel.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void serialize(UserModel value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeStartObject();
		gen.writeNumberField("userId", value.getUserID());
		gen.writeStringField("username", value.getUsername());
		gen.writeStringField("password", value.getPassword());
		gen.writeStringField("fName", value.getfName());
		gen.writeStringField("lName", value.getlName());
		gen.writeNumberField("userRole", value.getUserRole());
		if (value.gettList() != null) {
			gen.writeArrayFieldStart("taskList");
			for (TaskModel use : value.gettList()) {
				gen.writeStartObject();
				gen.writeStringField("taskName", use.getTaskName());
				gen.writeEndObject();
			}
			gen.writeEndArray();
		}
		if (value.getPmList() != null) {
			gen.writeArrayFieldStart("projectList");
			for (ProjectModel use : value.getPmList()) {
				gen.writeStartObject();
				gen.writeStringField("projectName", use.getProjectName());
				gen.writeEndObject();
			}
			gen.writeEndArray();
		}

		gen.writeEndObject();

	}

}
