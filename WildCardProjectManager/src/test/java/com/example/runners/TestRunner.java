package com.example.runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		//features = {"feature/RegLoginReset.feature"},
		features = {"feature"},
		glue = {"com.example.gluecode"}
		)

public class TestRunner {

}
