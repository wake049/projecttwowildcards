package com.example.page;


import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class MemberProjectView {
	
	@FindBy (xpath="//tr")
	private List<WebElement> taskViewRows;
	
	@FindBy ()
	private WebElement taskName;
	
	private WebElement description;
	
	//private WebElement statusTask;
	
	private WebElement tag;
	
	private WebElement dueDate;
	
	private WebElement priorModel;
	

	public MemberProjectView(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
	}
	
	public int sizeOfTable() {
		return this.taskViewRows.size();
		
	}
	
}
