package com.example.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateProject {
	
	@FindBy (xpath="//input[@id='projectName']")
	private WebElement projectName;
	
	@FindBy (xpath="//input[@id='description']")
	private WebElement  description;
	
	@FindBy (xpath="//input[@id='dueDate']")
	private WebElement dueDate;
	
	@FindBy (xpath="//input[@id='checkbox']")
	private WebElement checkBox;
		
	@FindBy (xpath="//button[@id='submit']")
	private WebElement submitButton;
	
	
	public CreateProject(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
	}

	public void toCreateProject(String projectName, String description, String dueDate, Boolean checkBox) {
		this.projectName.clear();
		this.description.clear();
		this.dueDate.clear();
		this.checkBox.clear();
		this.projectName.sendKeys(projectName);
		this.description.sendKeys(description);
		this.dueDate.sendKeys(dueDate);
		this.checkBox.click(); 
		this.submitButton.click();
	}
	

}
