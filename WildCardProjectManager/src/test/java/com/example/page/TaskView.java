package com.example.page;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TaskView {
	
	@FindBy (xpath="//tr")
	private List<WebElement> taskViewRows;
	
	@FindBy (xpath="//a[@id='TaskName']")  // should these be input tags?
	private WebElement taskName;
	
	@FindBy (xpath="//td[@id='description']")
	private WebElement  description;
	
	@FindBy (xpath="//td[@id='statusTask']") 
	private WebElement  statusTask;
	
	@FindBy (xpath="//td[@id='projectid']")
	private WebElement  tag;
		
	@FindBy (xpath="//td[@id='dueDate']")
	private WebElement dueDate;
	
	@FindBy (xpath="//td[@id='priorityString']")
	private WebElement priorityString;
	
	@FindBy (xpath="//a[@id='userList']") 
	private List<WebElement>  user;
	
	@FindBy(xpath="//a")
	private List<WebElement> taskAnchorTags;
	
	
	
	
	public TaskView(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
	}
	
	public int sizeOfTable() {
		return this.taskViewRows.size();
		
	}
	
	
	public void clickLink(String taskName) {
		for (WebElement link: this.taskAnchorTags) {
			if(link.getText().equals(taskName)) {
				link.click();
			}
		}
	}

	public List<WebElement> getTaskAnchorTags() {
		return taskAnchorTags;
	}
 }
