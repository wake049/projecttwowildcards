package com.example.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DeleteTask {
	
	
	@FindBy (xpath="//button[@id='deleteButton']")
	private WebElement deleteTaskButton;
	
	public DeleteTask(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
	}
	
	public void deleteTaskButton() {
		this.deleteTaskButton.click();
	}

}
