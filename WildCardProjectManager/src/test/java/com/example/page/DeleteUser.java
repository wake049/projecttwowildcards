package com.example.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DeleteUser {
	
	@FindBy (xpath="//button[@id='deleteButton']")
	private WebElement deleteUserButton;
	
	public DeleteUser(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
	}
	
	public void deleteUserButton() {
		this.deleteUserButton.click();
	}

		

}
