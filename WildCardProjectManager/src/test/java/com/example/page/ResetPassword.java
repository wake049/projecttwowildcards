package com.example.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ResetPassword {
	
	@FindBy (xpath="//input[@id='username']")
	private WebElement username;
	
	@FindBy (xpath="//input[@id='password-reset']")
	private WebElement  passwordReset;
	
	@FindBy (xpath="//button[@id='changePassword']")
	private WebElement submitButton;
	
	
	
	public ResetPassword(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
	}

	public void toResetPassword(String username, String passwordReset) {
		this.username.clear();
		this.passwordReset.clear();
		this.username.sendKeys(username);
		this.passwordReset.sendKeys(passwordReset);
		this.submitButton.click();
	}

}
