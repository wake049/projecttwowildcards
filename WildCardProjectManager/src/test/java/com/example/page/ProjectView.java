package com.example.page;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProjectView {
	
	@FindBy (xpath="//tr")
	private List<WebElement> projectViewRows;
	
	@FindBy ()
	private WebElement projectName;
	
	private WebElement description;
	
	private WebElement dueDate;
	
	private WebElement userList;
	
	private WebElement taskList;
	
	
	public ProjectView(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
	}
	
	public int sizeOfTable() {
		return this.projectViewRows.size();
		
	}
	
	

}
