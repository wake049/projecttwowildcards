package com.example.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateTask {
	
	@FindBy (xpath="//input[@id='TaskName']")
	private WebElement taskName;
	
	@FindBy (xpath="//input[@id='Description']")
	private WebElement  description;
	
	@FindBy (xpath="//select[@id='statusTask']") // created id field
	private WebElement  statusTask;
	
	@FindBy (xpath="//input[@id='dueDat']")
	private WebElement dueDate;
	
	@FindBy (xpath="//select[@id='priorityString']") // created id field
	private WebElement priorityString;
	
	@FindBy (xpath="//select[@id='projectName']") // created id field
	private WebElement  projectName2;
	
	@FindBy (xpath="//input[@id='projectid']")
	private WebElement  tag;
		
	@FindBy (xpath="//button[@id='submit']")
	private WebElement submitButton;
	
	@FindBy (xpath="//input[@id='checkbox']")
	private WebElement checkBox;
	
	public CreateTask(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
	}

	public void toCreateTask(String taskName, String description, Boolean statusTask, String dueDate, Boolean priorityString, Boolean projectName2, String tag) {
		this.taskName.clear();
		this.description.clear();
		this.statusTask.clear();
		this.dueDate.clear();
		this.priorityString.clear();
		this.projectName2.clear();
		this.tag.clear();
		this.taskName.sendKeys(taskName);
		this.description.sendKeys(description);
		this.statusTask.click();
		this.dueDate.sendKeys(dueDate);
		this.priorityString.click();
		this.projectName2.click();
		this.tag.sendKeys(tag);
		this.submitButton.click();
	}
	
	public void toUpdateTask(Boolean statusTask, Boolean priorityString, Boolean checkBox, String tag) {
		this.statusTask.clear();
		this.priorityString.clear();
		this.checkBox.clear();
		this.tag.clear();
		this.statusTask.click();
		this.priorityString.click();
		this.checkBox.click(); 
		this.tag.sendKeys(tag);
		this.submitButton.click();
	}

}
