package com.example.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegisterUser {
	
	@FindBy (xpath="//input[@id='FirstName']")
	private WebElement firstName;
	
	@FindBy (xpath="//input[@id='LastName']")
	private WebElement  lastName;
	
	@FindBy (xpath="//input[@id='email']")
	private WebElement email;
	
	@FindBy (xpath="//input[@id='psw']")
	private WebElement  password;
	
	@FindBy (xpath="//input[@id='psw-repeat']")
	private WebElement  passwordRepeat;
	
	@FindBy (xpath="//button[@id='registerButton']")
	private WebElement registerButton;
	
	
	public RegisterUser(WebDriver driver) {
		PageFactory.initElements(driver, this);
		
	}

	public void toRegisterUser(String firstName, String lastName, String email, String password,String passwordRepeat) {
		this.firstName.clear();
		this.lastName.clear();
		this.email.clear();
		this.password.clear();
		this.passwordRepeat.clear();
		this.firstName.sendKeys(firstName);
		this.lastName.sendKeys(lastName);
		this.email.sendKeys(email);
		this.password.sendKeys(password);
		this.passwordRepeat.sendKeys(passwordRepeat);
		this.registerButton.click();
	}
	
}
