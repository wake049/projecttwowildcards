package com.example.unit;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.model.UserModel;
import com.example.repo.UserRepo;
import com.example.service.UserService;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class UserTest {

	@InjectMocks
	UserService uServ;
	
	UserModel pMod;
	UserModel user;
	
	@MockBean
	UserRepo uRepo;
	
	@BeforeEach
	public void setup() throws Exception{}
	
	
	@Test
	public void insertUserTest() {
		user = new UserModel("Alex","Coly", 2,"Acoly@gmail.com","password",new ArrayList<>(), new ArrayList<>());
		when(uRepo.save(user)).thenReturn(user);
		uServ.insertUser(user);
		verify(uRepo, times(1)).save(user);
	}
	@Test
	public void getAllUserTest() {
		List<UserModel> uList = new ArrayList<>();
		uList.add(user);
		when(uRepo.findAll()).thenReturn(uList);
		assertEquals(uList, uServ.findAllUsers());
	}
	@Test
		public void deleteUserTest() {
		when(uRepo.save(user)).thenReturn(user);
		uServ.deleteUser(user);
		verify(uRepo, times(1)).delete(user);
	}
}
