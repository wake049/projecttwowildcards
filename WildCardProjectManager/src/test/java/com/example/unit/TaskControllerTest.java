package com.example.unit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.model.TaskModel;
import com.example.repo.TaskRepo;
import com.example.service.TaskService;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class TaskControllerTest {
	@InjectMocks
	TaskService tServ;
	
	TaskModel task;
	
	@MockBean
	TaskRepo tRepo;
	
	@BeforeEach
	public void setup() throws Exception {}
	
	@Test
	public void getAllTaskTest() {
		List<TaskModel> tList = new ArrayList<>();
		tList.add(task);
		when(tRepo.findAll()).thenReturn(tList);
		assertEquals(tList, tServ.getAllTasks());
	}
	
	@Test
	public void insertTaskTest() {
		task = new TaskModel();
		when(tRepo.save(task)).thenReturn(task);
		tServ.insertTask(task);
		verify(tRepo, times(1)).save(task);
	}
	
	@Test
	public void deleteTaskTest() {
		task = new TaskModel();
		when(tRepo.save(task)).thenReturn(task);
		tServ.deleteTask(task);
		verify(tRepo, times(1)).delete(task);
	}

}