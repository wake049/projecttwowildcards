package com.example.unit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;

import org.springframework.test.annotation.Rollback;


import com.example.model.ImageModel;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;


import com.example.model.ProjectModel;
import com.example.model.TaskModel;
import com.example.model.UserModel;
import com.example.repo.ProjectRepo;
import com.example.service.ProjectService;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class ProjectControllerTest {

	@InjectMocks
	ProjectService pServ;
	
	ProjectModel pMod;
	UserModel user;
	
	@MockBean
	ProjectRepo pRepo;
	
	
	@BeforeEach
	public void setup() throws Exception{
	
	}
	
	@Test
		public void testGetAllProject(){
		List<ProjectModel> pList = new ArrayList<>();
		pList.add(pMod);
		when(pRepo.findAll()).thenReturn(pList);
		assertEquals(pList, pServ.getAllProjects());
	}
	
	@Test
	public void insertProjectTest(){
	
		pMod = new ProjectModel();
		
		when(pRepo.save(pMod)).thenReturn(pMod);
		
		pServ.insertProject(pMod);
		
		verify(pRepo, times(1)).save(pMod);
	}
	
	@Test
	public void deleteProjectTest() {
		pMod = new ProjectModel();
		
		when(pRepo.save(pMod)).thenReturn(pMod);
		
		pServ.deleteProject(pMod);
		
		verify(pRepo, times(1)).delete(pMod);
	}

}