package com.example.gluecode;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.page.LoginPage;
import com.example.page.RegisterUser;
import com.example.page.ResetPassword;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class UserManagementTests {
	

	private LoginPage lp;
	private RegisterUser ru;
	private ResetPassword rp;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String passwordRepeat;
	private String username;
	
	
	@Given("user on the homepage")
	public void user_on_the_homepage() {
	    this.lp = new LoginPage(WildCardUtilityDriver.driver);
	    assertEquals("http://localhost:4200/login", WildCardUtilityDriver.driver.getCurrentUrl());
	}


	@Given("user selects the register new user link")
	public void user_selects_the_register_new_user_link() {
		WebElement register = WildCardUtilityDriver.driver.findElement(By.xpath("/html/body/app-root/app-user-login/div/form/div[4]/span/small[2]/a"));
		WebDriverWait wait = new WebDriverWait(WildCardUtilityDriver.driver, 5);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/app-root/app-user-login/div/form/div[4]/span/small[2]/a")));
		register.click();
		//assertEquals("http://localhost:4200/register", WildCardUtilityDriver.driver.getCurrentUrl());
		
	}
	@Then("the user is directed to the register user page")
	public void the_user_is_directed_to_the_register_user_page() {
		WebDriverWait wait = new WebDriverWait(WildCardUtilityDriver.driver, 5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("FirstName")));
		 assertEquals("http://localhost:4200/register", WildCardUtilityDriver.driver.getCurrentUrl());
		 
	}
	@Given("the user fills in the first name {string}")
	public void the_user_fills_in_the_first_name(String string) {
	    this.firstName=string;
	}
	@Given("the user fills in the last name {string}")
	public void the_user_fills_in_the_last_name(String string) {
	    this.lastName=string;
	}
	@Given("the user fills in their email {string}")
	public void the_user_fills_in_their_email(String string) {
	    this.email=string;
	}
	@Given("the user fills in their password {string}")
	public void the_user_fills_in_their_password(String string) {
	    this.password=string;
	}
	@Given("the user repeats filling in their password {string}")
	public void the_user_repeats_filling_in_their_password(String string) {
	    this.passwordRepeat=string;
	}
	@Given("user clicks register the button")
	public void user_clicks_register_the_button() {
		RegisterUser ru = new RegisterUser(WildCardUtilityDriver.driver);
	   ru.toRegisterUser(this.firstName, this.lastName, this.email, this.password, this.passwordRepeat);

	   
	}
	@Then("user will be taken to home page to login as a new users")
	public void user_will_be_taken_to_home_page_to_login_as_a_new_users() {
		assertEquals("http://localhost:4200/login", WildCardUtilityDriver.driver.getCurrentUrl());
	  //WebElement register = WildCardUtilityDriver.driver.findElement(By.xpath("/html/body/app-root/app-register/div/form/div[1]/button"));
	   //register.click();
		WebDriverWait wait = new WebDriverWait(WildCardUtilityDriver.driver, 5);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/app-user-login/div/form/div[4]/span/small[2]/a")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/app-user-login")));
		assertEquals("http://localhost:4200/login", WildCardUtilityDriver.driver.getCurrentUrl());
	}

	
	
//	@Given("user enters wrong characters")
//	public void user_enters_wrong_characters() {
//		this.lp.loginToHomePage(this.username, this.password);
//	}
	
	
	
	@Given("an error message displays with invalid credentials")
	public void an_error_message_displays_with_invalid_credentials() {
		 assertEquals("http://localhost:4200/notfound", WildCardUtilityDriver.driver.getCurrentUrl());
	
	}
	@Then("user is sent back on registration page")
	public void user_is_sent_back_on_registration_page() {
		 assertEquals("http://localhost:4200/login", WildCardUtilityDriver.driver.getCurrentUrl());
	}

	@Given("user on the homepage for login")
	public void user_on_the_homepage_for_login() {
		this.lp = new LoginPage(WildCardUtilityDriver.driver);
	    assertEquals("http://localhost:4200/login", WildCardUtilityDriver.driver.getCurrentUrl());
	}
	@Given("user enters their email as {string}")
	public void user_enters_their_email_as(String string) {
	    this.email=string;
	}
	@Given("user enters {string}")
	public void user_enters(String string) {
	    this.password=string;
	}
	@Given("user click login button")
	public void user_click_login_button() {
	   this.lp.loginToHomePage(this.username, this.password);
	}
	@Then("user should be directed to the applications main page")
	public void user_should_be_directed_to_the_applications_main_page() {
		WebDriverWait wait = new WebDriverWait(WildCardUtilityDriver.driver, 5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("task")));
		 assertEquals("http://localhost:4200/task", WildCardUtilityDriver.driver.getCurrentUrl());
	}

	
	@Given("user clicks login button")
	public void user_clicks_login_button() {
		this.lp.loginToHomePage(this.username, this.password);
	}
	
	@Given("error message displayed with wrong password")
	public void error_message_displayed_with_wrong_password() {
		
		 assertEquals("http://localhost:4200/notfound", WildCardUtilityDriver.driver.getCurrentUrl());
	}
	
	@Given("user is inside the application")
	public void user_is_inside_the_aplication() {
		//WebDriverWait wait = new WebDriverWait(WildCardUtilityDriver.driver, 5);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/app-project-view/nav")));
		 assertEquals("http://localhost:4200/task", WildCardUtilityDriver.driver.getCurrentUrl());
	}
	@Given("user selects the logout feature")
	public void user_selects_the_logout_feature() {
		WebDriverWait wait = new WebDriverWait(WildCardUtilityDriver.driver, 5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("loginButton")));
		assertEquals("http://localhost:4200/login", WildCardUtilityDriver.driver.getCurrentUrl());
	}
	@Then("all sessions are invalidated")
	public void all_sessions_are_invalidated() {
		assertEquals("http://localhost:4200/login", WildCardUtilityDriver.driver.getCurrentUrl());
	}
	@Then("user is sent back to the login page")
	public void user_is_sent_back_to_the_login_page() {
		 assertEquals("http://localhost:4200/login", WildCardUtilityDriver.driver.getCurrentUrl());
	}

	
	@Given("the user selects the forgot password link")
	public void the_user_selects_the_forgot_password_link() {
		assertEquals("http://localhost:4200/login", WildCardUtilityDriver.driver.getCurrentUrl());
		
	}
	@When("the user is directeed to the reset password page")
	public void the_user_is_directeed_to_the_reset_password_page() {
		WebDriverWait wait = new WebDriverWait(WildCardUtilityDriver.driver, 5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("FirstName")));
		 assertEquals("http://localhost:4200/forgotpassword", WildCardUtilityDriver.driver.getCurrentUrl());
	}
	@When("the user fills in their username {string}")
	public void the_user_fills_in_their_username(String string) {
	   this.username=string;
	}
	@When("the user fills in a new password {string} they would like to use")
	public void the_user_fills_in_a_new_password_they_would_like_to_use(String string) {
	  this.password=string;
	}
	@When("the user clicks the submit button")
	public void the_user_clicks_the_submit_button() {
		this.rp.toResetPassword(this.username, this.password);
	}
	@Then("the user should be directed back to the login page")
	public void the_user_should_be_directed_back_to_the_login_page() {
		WebDriverWait wait = new WebDriverWait(WildCardUtilityDriver.driver, 5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
		 assertEquals("http://localhost:4200/login", WildCardUtilityDriver.driver.getCurrentUrl());
	}
	@Then("the user can use their newly created password to log in")
	public void the_user_can_use_their_newly_created_password_to_log_in() {
		 this.lp = new LoginPage(WildCardUtilityDriver.driver);
		    assertEquals("http://localhost:4200/login", WildCardUtilityDriver.driver.getCurrentUrl());
	}


}

	
	