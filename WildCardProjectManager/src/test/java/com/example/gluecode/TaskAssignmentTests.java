package com.example.gluecode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.page.CreateProject;
import com.example.page.CreateTask;
import com.example.page.LoginPage;
import com.example.page.TaskView;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class TaskAssignmentTests {
	
	private LoginPage lp;
	private CreateProject cp;
	private CreateTask ct;
	private TaskView tv;
	private String password;
	private String username;
	private String projectName;
	private String description;
	private String dueDate;
	private Boolean checkBox;
	private String taskName;
	private Boolean statusTask;
	private Boolean priorityString;
	private Boolean projectName2;
	private String tag;
	
	
	
	@Given("The user is on the login page")
	public void the_user_is_on_the_login_page() {
		this.lp = new LoginPage(WildCardUtilityDriver.driver);
	    assertEquals("http://localhost:4200/login", WildCardUtilityDriver.driver.getCurrentUrl());
	}

	@When("the user inputs {string} into the username field")
	public void the_user_inputs_into_the_username_field(String string) {
	   this.username = string;
	}
	@When("the user inputs {string} in to password field")
	public void the_user_inputs_in_to_password_field(String string) {
	    this.password = string;
	}
	@When("the user clicks login")
	public void the_user_clicks_login() {
		this.lp.loginToHomePage(this.username, this.password);
	}
	@Then("the user is directed the App main page")
	public void the_user_is_directed_the_app_main_page() {
		assertEquals("http://localhost:4200/task", WildCardUtilityDriver.driver.getCurrentUrl());
		WebDriverWait wait = new WebDriverWait(WildCardUtilityDriver.driver, 5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("task")));
		 assertEquals("http://localhost:4200/task", WildCardUtilityDriver.driver.getCurrentUrl());
	}
	
	@Given("that a manager selects create a project")
	public void that_a_manager_selects_create_a_project() {
		WebDriverWait wait = new WebDriverWait(WildCardUtilityDriver.driver, 5); 
	    wait.until(ExpectedConditions.elementToBeClickable(By.id("submit"))); 
	    this.cp = new CreateProject(WildCardUtilityDriver.driver);
	}
	@Given("we arrive at the create project page")
	public void we_arrive_at_the_create_project_page() {
		 assertEquals("http://localhost:4200/create", WildCardUtilityDriver.driver.getCurrentUrl());
	}
	@When("the user inputs {string} in Project Name")
	public void the_user_inputs_in_project_name(String string) {
	   this.projectName = string;
	}
	@When("the user inputs {string} in Description")
	public void the_user_inputs_in_description(String string) {
	   this.description = string;
	}
	@When("the user inputs {string} in Due Date")
	public void the_user_inputs_in_due_date(String string) {
	    this.dueDate = string;
	}
	@When("the user checks the checkbox to add a team member")
	public void the_user_checks_the_checkbox_to_add_a_team_member() {
	    this.checkBox = true;
	}
	@When("the user clicks submit")
	public void the_user_clicks_submit() {
		this.cp.toCreateProject(this.projectName, this.description, this.dueDate, this.checkBox);
	}

	@Given("that a manager selects create a task")
	public void that_a_manager_selects_create_a_task() {
		WebDriverWait wait = new WebDriverWait(WildCardUtilityDriver.driver, 5); 
	    wait.until(ExpectedConditions.elementToBeClickable(By.id("submit"))); 
	    this.cp = new CreateProject(WildCardUtilityDriver.driver);
	}
	@Given("we arrive at the create task page")
	public void we_arrive_at_the_create_task_page() {
		 assertEquals("http://localhost:4200/taskcreate", WildCardUtilityDriver.driver.getCurrentUrl());
		 
	}
	@When("the user inputs {string} in Task Name")
	public void the_user_inputs_in_task_name(String string) {
	    this.taskName = string;
	}
	
	@When("the user selects the current status from the Status dropdown")
	public void the_user_selects_the_current_status_from_the_status_dropdown() {
	    this.statusTask = true;
	}
	
	@When("the user selects the current priority from the Priority dropdown")
	public void the_user_selects_the_current_priority_from_the_priority_dropdown() {
	   this.priorityString = true; 
	}
	@When("the user selects the current priority from the Project dropdown")
	public void the_user_selects_the_current_priority_from_the_project_dropdown() {
	   this.projectName2 = true;
	}
	@When("the user inputs {string} in Tag box")
	public void the_user_inputs_in_tag_box(String string) {
	    this.tag = string;
	}
	@When("the user clicks the submit Button")
	public void the_user_clicks_the_submit_Button() {
		this.ct.toCreateTask(this.taskName, this.description, this.statusTask, this.dueDate, this.priorityString, this.projectName2, this.tag);
	}
	@Given("that a manager selects view task")
	public void that_a_manager_selects_view_task() {
		WebDriverWait wait = new WebDriverWait(WildCardUtilityDriver.driver, 5); 
	    wait.until(ExpectedConditions.elementToBeClickable(By.tagName("a"))); 
	    this.tv = new TaskView(WildCardUtilityDriver.driver);
	}
	@Given("we arrive at the view task page")
	public void we_arrive_at_the_view_task_page() {
		 assertEquals("http://localhost:4200/taskmanager", WildCardUtilityDriver.driver.getCurrentUrl());
	}
	@Then("all tasks should be displayed")
	public void all_tasks_should_be_displayed() {
		assertTrue(this.tv.sizeOfTable()>1);
	    
	}
	
	@When("user clicks on link in first task displayed")  // review
	public void user_clicks_on_link_in_first_task_displayed() {
		assertEquals("http://localhost:4200/*", WildCardUtilityDriver.driver.getCurrentUrl());
	}
	@When("user arrives at the task edit page")
	public void user_arrives_at_the_task_edit_page() {
		assertEquals("http://localhost:4200/taskcreate/change/new", WildCardUtilityDriver.driver.getCurrentUrl());  
	  
	}
	
	@When("the user clicks submit task button")
	public void the_user_clicks_submit_task_button() {
		this.ct.toUpdateTask(this.statusTask, this.priorityString, this.checkBox, this.tag);
	}


	
//////===================Block out Below Only
	
//	@Given("that the project manager is at the project view page")
//	public void that_the_project_manager_is_at_the_project_view_page() {
//		assertEquals("http://localhost:4200/projects", WildCardUtilityDriver.driver.getCurrentUrl());
//		
//	}
//	@Given("the project manager wants to assign new tasks")
//	public void the_project_manager_wants_to_assign_new_tasks() {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
//	}
//	@When("the project manager assigns a task by outling details")
//	public void the_project_manager_assigns_a_task_by_outling_details() {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
//	}
//	@When("the manager hits enter the manager is prompted for other entries\\/assigments")
//	public void the_manager_hits_enter_the_manager_is_prompted_for_other_entries_assigments() {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
//	}
//	@When("the manager is complete")
//	public void the_manager_is_complete() {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
//	}
//	@When("the team members gets notified")
//	public void the_team_members_gets_notified() {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
//	}
//	@Then("the project manager is returned to the project view")
//	public void the_project_manager_is_returned_to_the_projeect_view() {
//		assertEquals("http://localhost:4200/projects", WildCardUtilityDriver.driver.getCurrentUrl());
//	}

//	@Given("That I can view all projects")
//	public void that_i_can_view_all_projects() {
//		assertEquals("http://localhost:4200/projects", WildCardUtilityDriver.driver.getCurrentUrl());
//		
//	}
//	@Given("I am logged on as a manager")
//	public void i_am_logged_on_as_a_manager() {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
//	}
//	@When("I click on a project")
//	public void i_click_on_a_project() {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
//	}
//	@When("I drag and drop a employee into it")
//	public void i_drag_and_drop_a_employee_into_it() {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
//	}
//	@Then("I can see the employee is added to the project")
//	public void i_can_see_the_employee_is_added_to_the_project() {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
//	}
	
	
//	@When("I drag and drop a employee out of it")
//	public void i_drag_and_drop_a_employee_out_of_it() {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
//	}
//	@Then("I can see the employee is remove from the project")
//	public void i_can_see_the_employee_is_remove_from_the_project() {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
//	}

	
}
